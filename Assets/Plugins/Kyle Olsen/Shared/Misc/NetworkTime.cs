﻿using UnityEngine.Networking;

[NetworkSettings(channel = 0, sendInterval = 1f)]
public class NetworkTime : NetworkBehaviour
{
    private static int current;
    public static float GameTime { get; private set; }
    public static float DeltaGameTime { get; private set; }

    public static int Current
    {
        get { return current; }

        set
        {
            current = value;
            var newGameTime = current / 1000f;
            DeltaGameTime = newGameTime - GameTime;
            GameTime = newGameTime;
        }
    }

    private int lastServerReceivedTime, lastLocalReceivedTime;

    public override void OnStartServer()
    {
        Current = NetworkTransport.GetNetworkTimestamp();
        InvokeRepeating(nameof(SetDirty), 1f, 1f);
    }

    private void SetDirty()
    {
        SetDirtyBit(1);
    }

    private void Update()
    {
        var now = NetworkTransport.GetNetworkTimestamp();

        if (isServer)
        {
            Current = now;
        }
        else
        {
            Current = lastServerReceivedTime + now - lastLocalReceivedTime;
        }
    }

    public override bool OnSerialize(NetworkWriter writer, bool initialState)
    {
        writer.Write(Current);
        return true;
    }

    public override void OnDeserialize(NetworkReader reader, bool initialState)
    {
        var time = reader.ReadInt32();

        if (initialState)
        {
            lastLocalReceivedTime = NetworkTransport.GetNetworkTimestamp();
            Current = lastServerReceivedTime = time;
        }
        else
        {
            byte error;
            var connection = NetworkManager.singleton.client.connection;
            var delayMs = NetworkTransport.GetRemoteDelayTimeMS(connection.hostId, connection.connectionId, time, out error);
            var newTime = time + delayMs;
            Current = newTime;
            lastLocalReceivedTime = NetworkTransport.GetNetworkTimestamp();
            lastServerReceivedTime = newTime;
        }
    }
}