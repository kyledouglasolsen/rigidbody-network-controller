﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

public class ByteTests : MonoBehaviour
{
    void Start()
    {
        Assert.raiseExceptions = true;

        var writer = new ByteWriter();
        var reader = new ByteReader();
        var guid = Guid.NewGuid();

        writer.Write(Color.red);
        writer.Write(new Color32(0, 255, 0, 255));
        writer.Write(guid);
        writer.Write(Vector2.one);
        writer.Write(Vector3.one);
        writer.Write((byte)222);
        writer.Write((decimal)1.123452131231231234123);
        writer.Write(2.2);
        writer.Write(22f);
        writer.Write(12345);
        writer.Write(123456789L);
        writer.Write((short)123);
        writer.Write(18139283U);
        writer.Write(12731293798123UL);
        writer.Write((ushort)31293);

        reader.Replace(writer.ToArray());

        try
        {
            Assert.AreEqual(reader.ReadColor(), Color.red);
            Assert.AreEqual(reader.ReadColor32(), new Color32(0, 255, 0, 255));
            Assert.AreEqual(reader.ReadGuid(), guid);
            Assert.AreEqual(reader.ReadVector2(), Vector2.one);
            Assert.AreEqual(reader.ReadVector3(), Vector3.one);
            Assert.AreEqual(reader.ReadByte(), (byte)222);
            Assert.AreEqual(reader.ReadDecimal(), (decimal)1.123452131231231234123);
            Assert.AreEqual(reader.ReadDouble(), 2.2);
            Assert.AreEqual(reader.ReadFloat(), 22f);
            Assert.AreEqual(reader.ReadInt(), 12345);
            Assert.AreEqual(reader.ReadLong(), 123456789L);
            Assert.AreEqual(reader.ReadShort(), (short)123);
            Assert.AreEqual(reader.ReadUInt(), 18139283U);
            Assert.AreEqual(reader.ReadULong(), 12731293798123UL);
            Assert.AreEqual(reader.ReadUShort(), (ushort)31293);
        }
        catch (Exception e)
        {
            throw e;
        }

        Debug.Log("Passed tests!");
    }
}