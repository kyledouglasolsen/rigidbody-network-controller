﻿using UnityEngine.Networking;

public static class NetworkIdentityExtensions
{
    public static int GetHistoryTime(this NetworkIdentity identity, int interpolateMilliseconds, float deltaTime)
    {
        byte error;
        var deltaMs = (int)(deltaTime * 1000f);
        var rtt = identity.hasAuthority || identity.connectionToClient == null ? 0 : NetworkTransport.GetCurrentRTT(0, identity.connectionToClient.connectionId, out error);
        return identity.hasAuthority ? NetworkTime.Current : NetworkTime.Current - interpolateMilliseconds - deltaMs - (rtt / 2);
    }
}