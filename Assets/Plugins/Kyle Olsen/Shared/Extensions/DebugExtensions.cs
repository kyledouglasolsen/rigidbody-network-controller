using System;
using UnityEngine;

public static class DebugExtensions
{
    #region DebugDrawFunctions

    public static void DebugPoint(Vector3 position, Color color, float scale = 1.0f, float duration = 0,
        bool depthTest = true)
    {
        color = color == default(Color) ? Color.white : color;

        Debug.DrawRay(position + Vector3.up*(scale*0.5f), -Vector3.up*scale, color, duration, depthTest);
        Debug.DrawRay(position + Vector3.right*(scale*0.5f), -Vector3.right*scale, color, duration, depthTest);
        Debug.DrawRay(position + Vector3.forward*(scale*0.5f), -Vector3.forward*scale, color, duration, depthTest);
    }

    public static void DebugPoint(Vector3 position, float scale = 1.0f, float duration = 0, bool depthTest = true)
    {
        DebugPoint(position, Color.white, scale, duration, depthTest);
    }

    public static void DebugBounds(Bounds bounds, Color color, float duration = 0, bool depthTest = true)
    {
        var center = bounds.center;

        var x = bounds.extents.x;
        var y = bounds.extents.y;
        var z = bounds.extents.z;

        var ruf = center + new Vector3(x, y, z);
        var rub = center + new Vector3(x, y, -z);
        var luf = center + new Vector3(-x, y, z);
        var lub = center + new Vector3(-x, y, -z);

        var rdf = center + new Vector3(x, -y, z);
        var rdb = center + new Vector3(x, -y, -z);
        var lfd = center + new Vector3(-x, -y, z);
        var lbd = center + new Vector3(-x, -y, -z);

        Debug.DrawLine(ruf, luf, color, duration, depthTest);
        Debug.DrawLine(ruf, rub, color, duration, depthTest);
        Debug.DrawLine(luf, lub, color, duration, depthTest);
        Debug.DrawLine(rub, lub, color, duration, depthTest);

        Debug.DrawLine(ruf, rdf, color, duration, depthTest);
        Debug.DrawLine(rub, rdb, color, duration, depthTest);
        Debug.DrawLine(luf, lfd, color, duration, depthTest);
        Debug.DrawLine(lub, lbd, color, duration, depthTest);

        Debug.DrawLine(rdf, lfd, color, duration, depthTest);
        Debug.DrawLine(rdf, rdb, color, duration, depthTest);
        Debug.DrawLine(lfd, lbd, color, duration, depthTest);
        Debug.DrawLine(lbd, rdb, color, duration, depthTest);
    }

    public static void DebugBounds(Bounds bounds, float duration = 0, bool depthTest = true)
    {
        DebugBounds(bounds, Color.white, duration, depthTest);
    }

    public static void DebugLocalCube(Transform transform, Vector3 size, Color color, Vector3 center = default(Vector3),
        float duration = 0, bool depthTest = true)
    {
        var lbb = transform.TransformPoint(center + -size*0.5f);
        var rbb = transform.TransformPoint(center + new Vector3(size.x, -size.y, -size.z)*0.5f);

        var lbf = transform.TransformPoint(center + new Vector3(size.x, -size.y, size.z)*0.5f);
        var rbf = transform.TransformPoint(center + new Vector3(-size.x, -size.y, size.z)*0.5f);

        var lub = transform.TransformPoint(center + new Vector3(-size.x, size.y, -size.z)*0.5f);
        var rub = transform.TransformPoint(center + new Vector3(size.x, size.y, -size.z)*0.5f);

        var luf = transform.TransformPoint(center + size*0.5f);
        var ruf = transform.TransformPoint(center + new Vector3(-size.x, size.y, size.z)*0.5f);

        Debug.DrawLine(lbb, rbb, color, duration, depthTest);
        Debug.DrawLine(rbb, lbf, color, duration, depthTest);
        Debug.DrawLine(lbf, rbf, color, duration, depthTest);
        Debug.DrawLine(rbf, lbb, color, duration, depthTest);

        Debug.DrawLine(lub, rub, color, duration, depthTest);
        Debug.DrawLine(rub, luf, color, duration, depthTest);
        Debug.DrawLine(luf, ruf, color, duration, depthTest);
        Debug.DrawLine(ruf, lub, color, duration, depthTest);

        Debug.DrawLine(lbb, lub, color, duration, depthTest);
        Debug.DrawLine(rbb, rub, color, duration, depthTest);
        Debug.DrawLine(lbf, luf, color, duration, depthTest);
        Debug.DrawLine(rbf, ruf, color, duration, depthTest);
    }

    public static void DebugLocalCube(Transform transform, Vector3 size, Vector3 center = default(Vector3),
        float duration = 0, bool depthTest = true)
    {
        DebugLocalCube(transform, size, Color.white, center, duration, depthTest);
    }

    public static void DebugLocalCube(Matrix4x4 space, Vector3 size, Color color, Vector3 center = default(Vector3),
        float duration = 0, bool depthTest = true)
    {
        color = color == default(Color) ? Color.white : color;

        var lbb = space.MultiplyPoint3x4(center + -size*0.5f);
        var rbb = space.MultiplyPoint3x4(center + new Vector3(size.x, -size.y, -size.z)*0.5f);

        var lbf = space.MultiplyPoint3x4(center + new Vector3(size.x, -size.y, size.z)*0.5f);
        var rbf = space.MultiplyPoint3x4(center + new Vector3(-size.x, -size.y, size.z)*0.5f);

        var lub = space.MultiplyPoint3x4(center + new Vector3(-size.x, size.y, -size.z)*0.5f);
        var rub = space.MultiplyPoint3x4(center + new Vector3(size.x, size.y, -size.z)*0.5f);

        var luf = space.MultiplyPoint3x4(center + size*0.5f);
        var ruf = space.MultiplyPoint3x4(center + new Vector3(-size.x, size.y, size.z)*0.5f);

        Debug.DrawLine(lbb, rbb, color, duration, depthTest);
        Debug.DrawLine(rbb, lbf, color, duration, depthTest);
        Debug.DrawLine(lbf, rbf, color, duration, depthTest);
        Debug.DrawLine(rbf, lbb, color, duration, depthTest);

        Debug.DrawLine(lub, rub, color, duration, depthTest);
        Debug.DrawLine(rub, luf, color, duration, depthTest);
        Debug.DrawLine(luf, ruf, color, duration, depthTest);
        Debug.DrawLine(ruf, lub, color, duration, depthTest);

        Debug.DrawLine(lbb, lub, color, duration, depthTest);
        Debug.DrawLine(rbb, rub, color, duration, depthTest);
        Debug.DrawLine(lbf, luf, color, duration, depthTest);
        Debug.DrawLine(rbf, ruf, color, duration, depthTest);
    }

    public static void DebugLocalCube(Matrix4x4 space, Vector3 size, Vector3 center = default(Vector3),
        float duration = 0, bool depthTest = true)
    {
        DebugLocalCube(space, size, Color.white, center, duration, depthTest);
    }

    public static void DebugCircle(Vector3 position, Vector3 up, Color color, float radius = 1.0f, float duration = 0,
        bool depthTest = true)
    {
        var _up = up.normalized*radius;
        var _forward = Vector3.Slerp(_up, -_up, 0.5f);
        var _right = Vector3.Cross(_up, _forward).normalized*radius;

        var matrix = new Matrix4x4();

        matrix[0] = _right.x;
        matrix[1] = _right.y;
        matrix[2] = _right.z;

        matrix[4] = _up.x;
        matrix[5] = _up.y;
        matrix[6] = _up.z;

        matrix[8] = _forward.x;
        matrix[9] = _forward.y;
        matrix[10] = _forward.z;

        var _lastPoint = position + matrix.MultiplyPoint3x4(new Vector3(Mathf.Cos(0), 0, Mathf.Sin(0)));
        var _nextPoint = Vector3.zero;

        color = color == default(Color) ? Color.white : color;

        for (var i = 0; i < 91; i++)
        {
            _nextPoint.x = Mathf.Cos(i*4*Mathf.Deg2Rad);
            _nextPoint.z = Mathf.Sin(i*4*Mathf.Deg2Rad);
            _nextPoint.y = 0;

            _nextPoint = position + matrix.MultiplyPoint3x4(_nextPoint);

            Debug.DrawLine(_lastPoint, _nextPoint, color, duration, depthTest);
            _lastPoint = _nextPoint;
        }
    }

    public static void DebugCircle(Vector3 position, Color color, float radius = 1.0f, float duration = 0,
        bool depthTest = true)
    {
        DebugCircle(position, Vector3.up, color, radius, duration, depthTest);
    }

    public static void DebugCircle(Vector3 position, Vector3 up, float radius = 1.0f, float duration = 0,
        bool depthTest = true)
    {
        DebugCircle(position, up, Color.white, radius, duration, depthTest);
    }

    public static void DebugCircle(Vector3 position, float radius = 1.0f, float duration = 0, bool depthTest = true)
    {
        DebugCircle(position, Vector3.up, Color.white, radius, duration, depthTest);
    }

    public static void DebugWireSphere(Vector3 position, Color color, float radius = 1.0f, float duration = 0,
        bool depthTest = true)
    {
        var angle = 10.0f;

        var x = new Vector3(position.x, position.y + radius*Mathf.Sin(0), position.z + radius*Mathf.Cos(0));
        var y = new Vector3(position.x + radius*Mathf.Cos(0), position.y, position.z + radius*Mathf.Sin(0));
        var z = new Vector3(position.x + radius*Mathf.Cos(0), position.y + radius*Mathf.Sin(0), position.z);

        Vector3 new_x;
        Vector3 new_y;
        Vector3 new_z;

        for (var i = 1; i < 37; i++)
        {
            new_x = new Vector3(position.x, position.y + radius*Mathf.Sin(angle*i*Mathf.Deg2Rad),
                position.z + radius*Mathf.Cos(angle*i*Mathf.Deg2Rad));
            new_y = new Vector3(position.x + radius*Mathf.Cos(angle*i*Mathf.Deg2Rad), position.y,
                position.z + radius*Mathf.Sin(angle*i*Mathf.Deg2Rad));
            new_z = new Vector3(position.x + radius*Mathf.Cos(angle*i*Mathf.Deg2Rad),
                position.y + radius*Mathf.Sin(angle*i*Mathf.Deg2Rad), position.z);

            Debug.DrawLine(x, new_x, color, duration, depthTest);
            Debug.DrawLine(y, new_y, color, duration, depthTest);
            Debug.DrawLine(z, new_z, color, duration, depthTest);

            x = new_x;
            y = new_y;
            z = new_z;
        }
    }

    public static void DebugWireSphere(Vector3 position, float radius = 1.0f, float duration = 0, bool depthTest = true)
    {
        DebugWireSphere(position, Color.white, radius, duration, depthTest);
    }

    public static void DebugCylinder(Vector3 start, Vector3 end, Color color, float radius = 1, float duration = 0,
        bool depthTest = true)
    {
        var up = (end - start).normalized*radius;
        var forward = Vector3.Slerp(up, -up, 0.5f);
        var right = Vector3.Cross(up, forward).normalized*radius;

        DebugCircle(start, up, color, radius, duration, depthTest);
        DebugCircle(end, -up, color, radius, duration, depthTest);
        DebugCircle((start + end)*0.5f, up, color, radius, duration, depthTest);

        Debug.DrawLine(start + right, end + right, color, duration, depthTest);
        Debug.DrawLine(start - right, end - right, color, duration, depthTest);

        Debug.DrawLine(start + forward, end + forward, color, duration, depthTest);
        Debug.DrawLine(start - forward, end - forward, color, duration, depthTest);

        Debug.DrawLine(start - right, start + right, color, duration, depthTest);
        Debug.DrawLine(start - forward, start + forward, color, duration, depthTest);

        Debug.DrawLine(end - right, end + right, color, duration, depthTest);
        Debug.DrawLine(end - forward, end + forward, color, duration, depthTest);
    }

    public static void DebugCylinder(Vector3 start, Vector3 end, float radius = 1, float duration = 0,
        bool depthTest = true)
    {
        DebugCylinder(start, end, Color.white, radius, duration, depthTest);
    }

    public static void DebugCone(Vector3 position, Vector3 direction, Color color, float angle = 45, float duration = 0,
        bool depthTest = true)
    {
        var length = direction.magnitude;

        var _forward = direction;
        var _up = Vector3.Slerp(_forward, -_forward, 0.5f);
        var _right = Vector3.Cross(_forward, _up).normalized*length;

        direction = direction.normalized;

        var slerpedVector = Vector3.Slerp(_forward, _up, angle/90.0f);

        float dist;
        var farPlane = new Plane(-direction, position + _forward);
        var distRay = new Ray(position, slerpedVector);

        farPlane.Raycast(distRay, out dist);

        Debug.DrawRay(position, slerpedVector.normalized*dist, color);
        Debug.DrawRay(position, Vector3.Slerp(_forward, -_up, angle/90.0f).normalized*dist, color, duration, depthTest);
        Debug.DrawRay(position, Vector3.Slerp(_forward, _right, angle/90.0f).normalized*dist, color, duration, depthTest);
        Debug.DrawRay(position, Vector3.Slerp(_forward, -_right, angle/90.0f).normalized*dist, color, duration,
            depthTest);

        DebugCircle(position + _forward, direction, color, (_forward - slerpedVector.normalized*dist).magnitude,
            duration, depthTest);
        DebugCircle(position + _forward*0.5f, direction, color,
            (_forward*0.5f - slerpedVector.normalized*(dist*0.5f)).magnitude, duration, depthTest);
    }

    public static void DebugCone(Vector3 position, Vector3 direction, float angle = 45, float duration = 0,
        bool depthTest = true)
    {
        DebugCone(position, direction, Color.white, angle, duration, depthTest);
    }

    public static void DebugCone(Vector3 position, Color color, float angle = 45, float duration = 0,
        bool depthTest = true)
    {
        DebugCone(position, Vector3.up, color, angle, duration, depthTest);
    }

    public static void DebugCone(Vector3 position, float angle = 45, float duration = 0, bool depthTest = true)
    {
        DebugCone(position, Vector3.up, Color.white, angle, duration, depthTest);
    }

    public static void DebugArrow(Vector3 position, Vector3 direction, Color color, float duration = 0,
        bool depthTest = true)
    {
        Debug.DrawRay(position, direction, color, duration, depthTest);
        DebugCone(position + direction, -direction*0.333f, color, 15, duration, depthTest);
    }

    public static void DebugArrow(Vector3 position, Vector3 direction, float duration = 0, bool depthTest = true)
    {
        DebugArrow(position, direction, Color.white, duration, depthTest);
    }

    public static void DebugCapsule(Vector3 start, float height, Color color, float radius = 1, int direction = 0,
        float duration = 0, bool depthTest = true)
    {
        var capsuleDirection = Vector3.up;

        if (direction == 0)
            capsuleDirection = Vector3.right;
        else if (direction == 1)
            capsuleDirection = Vector3.up;
        else if (direction == 2)
            capsuleDirection = Vector3.forward;

        var end = start + capsuleDirection.normalized*height;

        var up = (end - start).normalized*radius;
        var forward = Vector3.Slerp(up, -up, 0.5f);
        var right = Vector3.Cross(up, forward).normalized*radius;

        var sideLength = Mathf.Max(0, height*0.5f - radius);
        var middle = (end + start)*0.5f;

        start = middle + (start - middle).normalized*sideLength;
        end = middle + (end - middle).normalized*sideLength;

        DebugCircle(start, up, color, radius, duration, depthTest);
        DebugCircle(end, -up, color, radius, duration, depthTest);

        Debug.DrawLine(start + right, end + right, color, duration, depthTest);
        Debug.DrawLine(start - right, end - right, color, duration, depthTest);

        Debug.DrawLine(start + forward, end + forward, color, duration, depthTest);
        Debug.DrawLine(start - forward, end - forward, color, duration, depthTest);

        for (var i = 1; i < 26; i++)
        {
            Debug.DrawLine(Vector3.Slerp(right, -up, i/25.0f) + start, Vector3.Slerp(right, -up, (i - 1)/25.0f) + start,
                color, duration, depthTest);
            Debug.DrawLine(Vector3.Slerp(-right, -up, i/25.0f) + start,
                Vector3.Slerp(-right, -up, (i - 1)/25.0f) + start, color, duration, depthTest);
            Debug.DrawLine(Vector3.Slerp(forward, -up, i/25.0f) + start,
                Vector3.Slerp(forward, -up, (i - 1)/25.0f) + start, color, duration, depthTest);
            Debug.DrawLine(Vector3.Slerp(-forward, -up, i/25.0f) + start,
                Vector3.Slerp(-forward, -up, (i - 1)/25.0f) + start, color, duration, depthTest);

            Debug.DrawLine(Vector3.Slerp(right, up, i/25.0f) + end, Vector3.Slerp(right, up, (i - 1)/25.0f) + end, color,
                duration, depthTest);
            Debug.DrawLine(Vector3.Slerp(-right, up, i/25.0f) + end, Vector3.Slerp(-right, up, (i - 1)/25.0f) + end,
                color, duration, depthTest);
            Debug.DrawLine(Vector3.Slerp(forward, up, i/25.0f) + end, Vector3.Slerp(forward, up, (i - 1)/25.0f) + end,
                color, duration, depthTest);
            Debug.DrawLine(Vector3.Slerp(-forward, up, i/25.0f) + end, Vector3.Slerp(-forward, up, (i - 1)/25.0f) + end,
                color, duration, depthTest);
        }
    }

    public static void DebugCapsule(Vector3 start, float height, float radius = 1, int direction = 0, float duration = 0,
        bool depthTest = true)
    {
        DebugCapsule(start, height, Color.white, radius, direction, duration, depthTest);
    }

    #endregion

    #region GizmoDrawFunctions

    public static void DrawPoint(Vector3 position, Color color, float scale = 1.0f)
    {
        var oldColor = Gizmos.color;

        Gizmos.color = color;
        Gizmos.DrawRay(position + Vector3.up*(scale*0.5f), -Vector3.up*scale);
        Gizmos.DrawRay(position + Vector3.right*(scale*0.5f), -Vector3.right*scale);
        Gizmos.DrawRay(position + Vector3.forward*(scale*0.5f), -Vector3.forward*scale);

        Gizmos.color = oldColor;
    }

    public static void DrawPoint(Vector3 position, float scale = 1.0f)
    {
        DrawPoint(position, Color.white, scale);
    }

    public static void DrawBounds(Bounds bounds, Color color)
    {
        var center = bounds.center;

        var x = bounds.extents.x;
        var y = bounds.extents.y;
        var z = bounds.extents.z;

        var ruf = center + new Vector3(x, y, z);
        var rub = center + new Vector3(x, y, -z);
        var luf = center + new Vector3(-x, y, z);
        var lub = center + new Vector3(-x, y, -z);

        var rdf = center + new Vector3(x, -y, z);
        var rdb = center + new Vector3(x, -y, -z);
        var lfd = center + new Vector3(-x, -y, z);
        var lbd = center + new Vector3(-x, -y, -z);

        var oldColor = Gizmos.color;
        Gizmos.color = color;

        Gizmos.DrawLine(ruf, luf);
        Gizmos.DrawLine(ruf, rub);
        Gizmos.DrawLine(luf, lub);
        Gizmos.DrawLine(rub, lub);

        Gizmos.DrawLine(ruf, rdf);
        Gizmos.DrawLine(rub, rdb);
        Gizmos.DrawLine(luf, lfd);
        Gizmos.DrawLine(lub, lbd);

        Gizmos.DrawLine(rdf, lfd);
        Gizmos.DrawLine(rdf, rdb);
        Gizmos.DrawLine(lfd, lbd);
        Gizmos.DrawLine(lbd, rdb);

        Gizmos.color = oldColor;
    }

    public static void DrawBounds(Bounds bounds)
    {
        DrawBounds(bounds, Color.white);
    }

    public static void DrawLocalCube(Transform transform, Vector3 size, Color color, Vector3 center = default(Vector3))
    {
        var oldColor = Gizmos.color;
        Gizmos.color = color;

        var lbb = transform.TransformPoint(center + -size*0.5f);
        var rbb = transform.TransformPoint(center + new Vector3(size.x, -size.y, -size.z)*0.5f);

        var lbf = transform.TransformPoint(center + new Vector3(size.x, -size.y, size.z)*0.5f);
        var rbf = transform.TransformPoint(center + new Vector3(-size.x, -size.y, size.z)*0.5f);

        var lub = transform.TransformPoint(center + new Vector3(-size.x, size.y, -size.z)*0.5f);
        var rub = transform.TransformPoint(center + new Vector3(size.x, size.y, -size.z)*0.5f);

        var luf = transform.TransformPoint(center + size*0.5f);
        var ruf = transform.TransformPoint(center + new Vector3(-size.x, size.y, size.z)*0.5f);

        Gizmos.DrawLine(lbb, rbb);
        Gizmos.DrawLine(rbb, lbf);
        Gizmos.DrawLine(lbf, rbf);
        Gizmos.DrawLine(rbf, lbb);

        Gizmos.DrawLine(lub, rub);
        Gizmos.DrawLine(rub, luf);
        Gizmos.DrawLine(luf, ruf);
        Gizmos.DrawLine(ruf, lub);

        Gizmos.DrawLine(lbb, lub);
        Gizmos.DrawLine(rbb, rub);
        Gizmos.DrawLine(lbf, luf);
        Gizmos.DrawLine(rbf, ruf);

        Gizmos.color = oldColor;
    }

    public static void DrawLocalCube(Transform transform, Vector3 size, Vector3 center = default(Vector3))
    {
        DrawLocalCube(transform, size, Color.white, center);
    }

    public static void DrawLocalCube(Matrix4x4 space, Vector3 size, Color color, Vector3 center = default(Vector3))
    {
        var oldColor = Gizmos.color;
        Gizmos.color = color;

        var lbb = space.MultiplyPoint3x4(center + -size*0.5f);
        var rbb = space.MultiplyPoint3x4(center + new Vector3(size.x, -size.y, -size.z)*0.5f);

        var lbf = space.MultiplyPoint3x4(center + new Vector3(size.x, -size.y, size.z)*0.5f);
        var rbf = space.MultiplyPoint3x4(center + new Vector3(-size.x, -size.y, size.z)*0.5f);

        var lub = space.MultiplyPoint3x4(center + new Vector3(-size.x, size.y, -size.z)*0.5f);
        var rub = space.MultiplyPoint3x4(center + new Vector3(size.x, size.y, -size.z)*0.5f);

        var luf = space.MultiplyPoint3x4(center + size*0.5f);
        var ruf = space.MultiplyPoint3x4(center + new Vector3(-size.x, size.y, size.z)*0.5f);

        Gizmos.DrawLine(lbb, rbb);
        Gizmos.DrawLine(rbb, lbf);
        Gizmos.DrawLine(lbf, rbf);
        Gizmos.DrawLine(rbf, lbb);

        Gizmos.DrawLine(lub, rub);
        Gizmos.DrawLine(rub, luf);
        Gizmos.DrawLine(luf, ruf);
        Gizmos.DrawLine(ruf, lub);

        Gizmos.DrawLine(lbb, lub);
        Gizmos.DrawLine(rbb, rub);
        Gizmos.DrawLine(lbf, luf);
        Gizmos.DrawLine(rbf, ruf);

        Gizmos.color = oldColor;
    }

    public static void DrawLocalCube(Matrix4x4 space, Vector3 size, Vector3 center = default(Vector3))
    {
        DrawLocalCube(space, size, Color.white, center);
    }

    public static void DrawCircle(Vector3 position, Vector3 up, Color color, float radius = 1.0f)
    {
        up = (up == Vector3.zero ? Vector3.up : up).normalized*radius;
        var _forward = Vector3.Slerp(up, -up, 0.5f);
        var _right = Vector3.Cross(up, _forward).normalized*radius;

        var matrix = new Matrix4x4();

        matrix[0] = _right.x;
        matrix[1] = _right.y;
        matrix[2] = _right.z;

        matrix[4] = up.x;
        matrix[5] = up.y;
        matrix[6] = up.z;

        matrix[8] = _forward.x;
        matrix[9] = _forward.y;
        matrix[10] = _forward.z;

        var _lastPoint = position + matrix.MultiplyPoint3x4(new Vector3(Mathf.Cos(0), 0, Mathf.Sin(0)));
        var _nextPoint = Vector3.zero;

        var oldColor = Gizmos.color;
        Gizmos.color = color == default(Color) ? Color.white : color;

        for (var i = 0; i < 91; i++)
        {
            _nextPoint.x = Mathf.Cos(i*4*Mathf.Deg2Rad);
            _nextPoint.z = Mathf.Sin(i*4*Mathf.Deg2Rad);
            _nextPoint.y = 0;

            _nextPoint = position + matrix.MultiplyPoint3x4(_nextPoint);

            Gizmos.DrawLine(_lastPoint, _nextPoint);
            _lastPoint = _nextPoint;
        }

        Gizmos.color = oldColor;
    }

    public static void DrawCircle(Vector3 position, Color color, float radius = 1.0f)
    {
        DrawCircle(position, Vector3.up, color, radius);
    }

    public static void DrawCircle(Vector3 position, Vector3 up, float radius = 1.0f)
    {
        DrawCircle(position, position, Color.white, radius);
    }

    public static void DrawCircle(Vector3 position, float radius = 1.0f)
    {
        DrawCircle(position, Vector3.up, Color.white, radius);
    }

    public static void DrawCylinder(Vector3 start, Vector3 end, Color color, float radius = 1.0f)
    {
        var up = (end - start).normalized*radius;
        var forward = Vector3.Slerp(up, -up, 0.5f);
        var right = Vector3.Cross(up, forward).normalized*radius;

        DrawCircle(start, up, color, radius);
        DrawCircle(end, -up, color, radius);
        DrawCircle((start + end)*0.5f, up, color, radius);

        var oldColor = Gizmos.color;
        Gizmos.color = color;

        Gizmos.DrawLine(start + right, end + right);
        Gizmos.DrawLine(start - right, end - right);

        Gizmos.DrawLine(start + forward, end + forward);
        Gizmos.DrawLine(start - forward, end - forward);

        Gizmos.DrawLine(start - right, start + right);
        Gizmos.DrawLine(start - forward, start + forward);

        Gizmos.DrawLine(end - right, end + right);
        Gizmos.DrawLine(end - forward, end + forward);

        Gizmos.color = oldColor;
    }

    public static void DrawCylinder(Vector3 start, Vector3 end, float radius = 1.0f)
    {
        DrawCylinder(start, end, Color.white, radius);
    }

    public static void DrawCone(Vector3 position, Vector3 direction, Color color, float angle = 45)
    {
        var length = direction.magnitude;

        var _forward = direction;
        var _up = Vector3.Slerp(_forward, -_forward, 0.5f);
        var _right = Vector3.Cross(_forward, _up).normalized*length;

        direction = direction.normalized;

        var slerpedVector = Vector3.Slerp(_forward, _up, angle/90.0f);

        float dist;
        var farPlane = new Plane(-direction, position + _forward);
        var distRay = new Ray(position, slerpedVector);

        farPlane.Raycast(distRay, out dist);

        var oldColor = Gizmos.color;
        Gizmos.color = color;

        Gizmos.DrawRay(position, slerpedVector.normalized*dist);
        Gizmos.DrawRay(position, Vector3.Slerp(_forward, -_up, angle/90.0f).normalized*dist);
        Gizmos.DrawRay(position, Vector3.Slerp(_forward, _right, angle/90.0f).normalized*dist);
        Gizmos.DrawRay(position, Vector3.Slerp(_forward, -_right, angle/90.0f).normalized*dist);

        DrawCircle(position + _forward, direction, color, (_forward - slerpedVector.normalized*dist).magnitude);
        DrawCircle(position + _forward*0.5f, direction, color,
            (_forward*0.5f - slerpedVector.normalized*(dist*0.5f)).magnitude);

        Gizmos.color = oldColor;
    }

    public static void DrawCone(Vector3 position, Vector3 direction, float angle = 45)
    {
        DrawCone(position, direction, Color.white, angle);
    }

    public static void DrawCone(Vector3 position, Color color, float angle = 45)
    {
        DrawCone(position, Vector3.up, color, angle);
    }

    public static void DrawCone(Vector3 position, float angle = 45)
    {
        DrawCone(position, Vector3.up, Color.white, angle);
    }

    public static void DrawArrow(Vector3 position, Vector3 direction, Color color)
    {
        var oldColor = Gizmos.color;
        Gizmos.color = color;

        Gizmos.DrawRay(position, direction);
        DrawCone(position + direction, -direction*0.333f, color, 15);

        Gizmos.color = oldColor;
    }

    public static void DrawArrow(Vector3 position, Vector3 direction)
    {
        DrawArrow(position, direction, Color.white);
    }

    public static void DrawCapsule(Vector3 start, float height, Color color, float radius = 1f, int direction = 0)
    {
        var capsuleDirection = Vector3.up;

        if (direction == 0)
            capsuleDirection = Vector3.right;
        else if (direction == 1)
            capsuleDirection = Vector3.up;
        else if (direction == 2)
            capsuleDirection = Vector3.forward;

        var end = start + capsuleDirection.normalized*height;

        var up = (end - start).normalized*radius;
        var forward = Vector3.Slerp(up, -up, 0.5f);
        var right = Vector3.Cross(up, forward).normalized*radius;

        var oldColor = Gizmos.color;
        Gizmos.color = color;

        var sideLength = Mathf.Max(0, height*0.5f - radius);
        var middle = (end + start)*0.5f;

        start = middle + (start - middle).normalized*sideLength;
        end = middle + (end - middle).normalized*sideLength;

        DrawCircle(start, up, color, radius);
        DrawCircle(end, -up, color, radius);

        Gizmos.DrawLine(start + right, end + right);
        Gizmos.DrawLine(start - right, end - right);

        Gizmos.DrawLine(start + forward, end + forward);
        Gizmos.DrawLine(start - forward, end - forward);

        for (var i = 1; i < 26; i++)
        {
            Gizmos.DrawLine(Vector3.Slerp(right, -up, i/25.0f) + start, Vector3.Slerp(right, -up, (i - 1)/25.0f) + start);
            Gizmos.DrawLine(Vector3.Slerp(-right, -up, i/25.0f) + start,
                Vector3.Slerp(-right, -up, (i - 1)/25.0f) + start);
            Gizmos.DrawLine(Vector3.Slerp(forward, -up, i/25.0f) + start,
                Vector3.Slerp(forward, -up, (i - 1)/25.0f) + start);
            Gizmos.DrawLine(Vector3.Slerp(-forward, -up, i/25.0f) + start,
                Vector3.Slerp(-forward, -up, (i - 1)/25.0f) + start);

            Gizmos.DrawLine(Vector3.Slerp(right, up, i/25.0f) + end, Vector3.Slerp(right, up, (i - 1)/25.0f) + end);
            Gizmos.DrawLine(Vector3.Slerp(-right, up, i/25.0f) + end, Vector3.Slerp(-right, up, (i - 1)/25.0f) + end);
            Gizmos.DrawLine(Vector3.Slerp(forward, up, i/25.0f) + end, Vector3.Slerp(forward, up, (i - 1)/25.0f) + end);
            Gizmos.DrawLine(Vector3.Slerp(-forward, up, i/25.0f) + end, Vector3.Slerp(-forward, up, (i - 1)/25.0f) + end);
        }

        Gizmos.color = oldColor;
    }

    public static void DrawCapsule(Vector3 start, float height, float radius = 1, int direction = 0)
    {
        DrawCapsule(start, height, Color.white, radius, direction);
    }

    #endregion

    #region DebugFunctions

    public static string MethodsOfObject(object obj, bool includeInfo = false)
    {
        var methods = "";
        var methodInfos = obj.GetType().GetMethods();
        for (var i = 0; i < methodInfos.Length; i++)
        {
            if (includeInfo)
                methods += methodInfos[i] + "\n";

            else methods += methodInfos[i].Name + "\n";
        }

        return methods;
    }

    public static string MethodsOfType(Type type, bool includeInfo = false)
    {
        var methods = "";
        var methodInfos = type.GetMethods();
        for (var i = 0; i < methodInfos.Length; i++)
        {
            if (includeInfo)
                methods += methodInfos[i] + "\n";

            else methods += methodInfos[i].Name + "\n";
        }

        return methods;
    }

    #endregion
}