﻿using UnityEngine;

public static class MathfHelpers
{
    public static float ClampAngle(float angle, float min, float max, bool wrap = true)
    {
        if (wrap)
        {
            while (angle < 0)
            {
                angle += 360;
            }

            while (angle >= 360f)
            {
                angle -= 360f;
            }
        }

        return Mathf.Clamp(angle, min, max);
    }
}