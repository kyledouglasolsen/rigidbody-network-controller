﻿using System;
using System.Collections.Generic;

public class PooledQueue<T> : Queue<T>
{
    private readonly Func<T> createCallback;

    public PooledQueue(Func<T> createCallback, int preAllocate = 0)
    {
        this.createCallback = createCallback;

        if (createCallback != null)
        {
            for (var i = 0; i < preAllocate; ++i)
            {
                Enqueue(createCallback());
            }
        }
    }

    public new T Dequeue()
    {
        return Count < 1 ? createCallback() : base.Dequeue();
    }
}