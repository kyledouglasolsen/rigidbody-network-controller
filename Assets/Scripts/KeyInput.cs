﻿using System;
using UnityEngine;

[Flags]
public enum KeyInput : uint
{
    None = 0u,
    Move_Left = 1u,
    Move_Right = 2u,
    Move_Forward = 4u,
    Move_Backward = 8u,
    Move_Jump = 16u,
    UseItem = 32u,
    NextItem = 64u,
    PreviousItem = 128u
}

public static class KeyInputExtensions
{
    public static KeyInput GetCurrent()
    {
        var key = KeyInput.None;

        if (Input.GetKey(KeyCode.W))
        {
            key |= KeyInput.Move_Forward;
        }

        if (Input.GetKey(KeyCode.A))
        {
            key |= KeyInput.Move_Left;
        }

        if (Input.GetKey(KeyCode.S))
        {
            key |= KeyInput.Move_Backward;
        }

        if (Input.GetKey(KeyCode.D))
        {
            key |= KeyInput.Move_Right;
        }

        if (Input.GetKey(KeyCode.Space))
        {
            key |= KeyInput.Move_Jump;
        }

        if (Input.GetKey(KeyCode.Mouse0))
        {
            key |= KeyInput.UseItem;
        }

        var scrollWheel = Input.GetAxis("Mouse ScrollWheel");

        if (scrollWheel > 0f)
        {
            key |= KeyInput.NextItem;
        }

        if (scrollWheel < 0f)
        {
            key |= KeyInput.PreviousItem;
        }

        return key;
    }

    public static Vector3 AsInputVelocity(this KeyInput keyInput)
    {
        var velocity = Vector3.zero;

        if (keyInput.HasFlag(KeyInput.Move_Forward))
        {
            velocity += Vector3.forward;
        }

        if (keyInput.HasFlag(KeyInput.Move_Left))
        {
            velocity += Vector3.left;
        }

        if (keyInput.HasFlag(KeyInput.Move_Backward))
        {
            velocity += Vector3.back;
        }

        if (keyInput.HasFlag(KeyInput.Move_Right))
        {
            velocity += Vector3.right;
        }

        return velocity.normalized;
    }
}