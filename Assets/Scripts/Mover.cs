﻿using UnityEngine;

public class Mover : MonoBehaviour, INeedNetworkInput, IAmAuthoritative, ICareAboutDeath
{
    [SerializeField] private float groundSpeed = 6f, airSpeed = 2f;
    [SerializeField] private float jumpSpeed = 8f;
    [SerializeField] private Vector3 gravity = Physics.gravity;
    private Vector3 velocity = Vector3.zero, airNudge;
    private bool nudged;
    private SimpleController controller;
    private BuffManager buffManager;

    public Vector3 Velocity => velocity;
    public bool IsGrounded => controller.IsGrounded;

    public bool Enabled => enabled;
    public bool OnlyOwnerUpdate { get; } = true;

    private void Awake()
    {
        buffManager = GetComponent<BuffManager>();
        controller = GetComponent<SimpleController>();
    }

    public void OnInput(Vector2 mouseInput, KeyInput keyInput, float deltaTime)
    {
        var move = new MoveSpeedBuff(1f, 0);
        var slow = new SlowDebuff(1f, 0);
        var externalMultiplier = buffManager.GetCombined(ref move).Multiplier * buffManager.GetCombined(ref slow).Multiplier;

        if (IsGrounded)
        {
            nudged = false;
            velocity = transform.TransformDirection(keyInput.AsInputVelocity()) * groundSpeed * externalMultiplier;

            if (keyInput.HasFlag(KeyInput.Move_Jump))
            {
                velocity.y = jumpSpeed;
            }
        }
        else if (!nudged)
        {
            var nudgeVelocity = transform.TransformDirection(keyInput.AsInputVelocity()) * airSpeed;

            if (nudgeVelocity != Vector3.zero)
            {
                velocity += nudgeVelocity;
                nudged = true;
            }
        }

        velocity += gravity * deltaTime;
        velocity = velocity.Round(3);
        var horizontal = new Vector3(velocity.x, 0f, velocity.z);
        var vertical = new Vector3(0f, velocity.y, 0f);

        controller.Move(horizontal * deltaTime);
        controller.Move(vertical * deltaTime);
        controller.transform.position = transform.position.Round(3);
    }

    public IAuthorityState GetState()
    {
        return new MoverState(transform.position, velocity, IsGrounded);
    }

    public void ApplyState(IAuthorityState state)
    {
        var moverState = (MoverState)state;
        velocity = moverState.Velocity;
        controller.SetGrounded(moverState.IsGrounded);
        transform.position = moverState.Position;
    }

    public void SerializeState(ByteWriter writer)
    {
        writer.Write(transform.position);
        writer.Write(velocity);
        writer.Write(IsGrounded);
    }

    public IAuthorityState DeserializeState(ByteReader reader)
    {
        return new MoverState(reader.ReadVector3(), reader.ReadVector3(), reader.ReadBool());
    }

    public void OnDie()
    {
        enabled = false;
    }

    public void OnRespawn()
    {
        enabled = true;
    }

    public struct MoverState : IAuthorityState
    {
        public MoverState(Vector3 position, Vector3 velocity, bool isGrounded)
        {
            Position = position;
            Velocity = velocity;
            IsGrounded = isGrounded;
        }

        public readonly Vector3 Position;
        public readonly Vector3 Velocity;
        public readonly bool IsGrounded;

        public int CalculateStateHash()
        {
            return Position.GetHashCode() ^ Velocity.GetHashCode() ^ IsGrounded.GetHashCode();
        }
    }
}