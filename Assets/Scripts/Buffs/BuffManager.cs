﻿using System.Collections.Generic;
using UnityEngine;

public class BuffManager : MonoBehaviour
{
    private readonly List<IBuff> buffs = new List<IBuff>();

    public T GetCombined<T>(ref T combined) where T : IBuff
    {
        for (var i = 0; i < buffs.Count; ++i)
        {
            if (buffs[i].GetType() == typeof(T))
            {
                combined = (T)combined.Combine(buffs[i]);
            }
        }

        return combined;
    }

    public void Add(IBuff buff)
    {
        if (!buff.AllowDuplicates)
        {
            buffs.RemoveAll(x => x.GetType() == buff.GetType());
        }

        buffs.Add(buff);
        enabled = true;
    }

    public void Remove(IBuff buff)
    {
        buffs.Remove(buff);
        enabled = buffs.Count > 0;
    }

    private void Update()
    {
        var now = NetworkTime.Current;

        for (var i = buffs.Count - 1; i >= 0; --i)
        {
            if (now >= buffs[i].EndTime)
            {
                buffs.RemoveAt(i);
            }
        }

        enabled = buffs.Count > 0;
    }
}