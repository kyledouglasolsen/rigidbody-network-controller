﻿public interface IBuff
{
    int EndTime { get; }
    bool AllowDuplicates { get; }
    
    IBuff Combine(IBuff otherBuff);
}