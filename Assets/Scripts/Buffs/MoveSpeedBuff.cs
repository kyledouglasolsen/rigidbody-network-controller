﻿using UnityEngine;

public struct MoveSpeedBuff : IBuff
{
    public float Multiplier { get; }
    public int EndTime { get; }
    public bool AllowDuplicates { get; }

    public MoveSpeedBuff(float multiplier, int endTime, bool allowDuplicates = false)
    {
        Multiplier = multiplier;
        EndTime = endTime;
        AllowDuplicates = allowDuplicates;
    }

    public IBuff Combine(IBuff otherBuff)
    {
        var otherMoveSpeedBuff = (MoveSpeedBuff)otherBuff;
        return new MoveSpeedBuff(Mathf.Max(Multiplier, otherMoveSpeedBuff.Multiplier), Mathf.Max(EndTime, otherBuff.EndTime));
    }
}