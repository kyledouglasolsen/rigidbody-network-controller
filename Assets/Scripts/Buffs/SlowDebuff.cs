﻿using UnityEngine;

public struct SlowDebuff : IBuff
{
    public float Multiplier { get; }
    public int EndTime { get; }
    public bool AllowDuplicates { get; }

    public SlowDebuff(float multiplier, int endTime, bool allowDuplicates = false)
    {
        Multiplier = multiplier;
        EndTime = endTime;
        AllowDuplicates = allowDuplicates;
    }

    public IBuff Combine(IBuff otherBuff)
    {
        var otherMoveSpeedBuff = (SlowDebuff)otherBuff;
        return new SlowDebuff(Mathf.Min(Multiplier, otherMoveSpeedBuff.Multiplier), Mathf.Max(EndTime, otherBuff.EndTime));
    }
}