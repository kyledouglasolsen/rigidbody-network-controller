﻿using UnityEngine;
using UnityEngine.Networking;

public class FragGrenade : Grenade
{
    [SerializeField] private float maxForceToApply = 1f;
    [SerializeField] private int maxDamageToApply = 5;

    protected override void OnHitCollider(AuthoritativePlayer owner, Collider hitCollider, Vector3 worldPoint, Vector3 directionTo, float distancePercent)
    {
        var damageable = hitCollider.GetComponent<IDamageable>();
        if (damageable != null)
        {
            var position = hitCollider.transform.InverseTransformPoint(worldPoint);
            damageable.DoDamage(new DamageInfo(owner, (uint)Mathf.Clamp(distancePercent * maxDamageToApply, 1, maxDamageToApply), position));
        }

        var rBody = hitCollider.attachedRigidbody;
        if (rBody != null)
        {
            var velocity = directionTo.normalized * distancePercent * maxForceToApply;
            rBody.AddForce(velocity, ForceMode.Impulse);
        }
    }
}