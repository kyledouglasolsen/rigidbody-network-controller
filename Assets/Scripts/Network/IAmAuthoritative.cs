﻿public interface IAmAuthoritative
{
    IAuthorityState GetState();
    void ApplyState(IAuthorityState state);

    void SerializeState(ByteWriter writer);
    IAuthorityState DeserializeState(ByteReader reader);
}