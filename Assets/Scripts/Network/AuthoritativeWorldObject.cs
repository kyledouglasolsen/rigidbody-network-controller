﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[NetworkSettings(channel = 1, sendInterval = 0.1f)]
public abstract class AuthoritativeWorldObject : NetworkBehaviour
{
    private static readonly List<AuthoritativeWorldObject> Empty = new List<AuthoritativeWorldObject>();
    public static readonly List<AuthoritativeWorldObject> All = new List<AuthoritativeWorldObject>();
    public static readonly Dictionary<uint, List<AuthoritativeWorldObject>> Lookup = new Dictionary<uint, List<AuthoritativeWorldObject>>();

    public static List<AuthoritativeWorldObject> Get(uint id)
    {
        List<AuthoritativeWorldObject> objects;
        return Lookup.TryGetValue(id, out objects) ? objects : Empty;
    }

    [SerializeField] private NetworkIdentity identity;
    public NetworkIdentity Identity => identity;

    public IHistoryEventTimeline Timeline { get; private set; }
    public IHistoryEventInterpolate Interpolate { get; private set; }

    public void DoInterpolate(int time)
    {
        if (!hasAuthority)
        {
            Interpolate.ApplyState(time);
        }
    }

    public override void OnStartServer()
    {
        TryAdd();
    }

    public override void OnStartClient()
    {
        TryAdd();
    }

    public override bool OnSerialize(NetworkWriter writer, bool initialState)
    {
        return true;
    }

    public override void OnDeserialize(NetworkReader reader, bool initialState)
    {
    }

    public abstract bool IsIdle();
    public abstract void ApplyEvent(ITimelineEvent timelineEvent);
    public abstract void AddEvent(int time, ITimelineEvent timelineEvent);
    public abstract ITimelineEvent ToEvent();

    public abstract void Write(ByteWriter writer);
    public abstract ITimelineEvent Read(ByteReader reader);

    protected abstract IHistoryEventTimeline CreateTimeline();
    protected abstract IHistoryEventInterpolate CreateInterpolate(IHistoryEventTimeline timeline);

    private void TryAdd()
    {
        List<AuthoritativeWorldObject> list;

        if (!Lookup.TryGetValue(identity.netId.Value, out list))
        {
            list = new List<AuthoritativeWorldObject>();
            Lookup[identity.netId.Value] = list;
        }

        if (!list.Contains(this))
        {
            Timeline = Timeline ?? CreateTimeline();
            Timeline.Clear();
            Interpolate = Interpolate ?? CreateInterpolate(Timeline);

            list.Add(this);
            All.Add(this);
        }
    }

    private void OnDisable()
    {
        List<AuthoritativeWorldObject> list;

        if (Lookup.TryGetValue(identity.netId.Value, out list))
        {
            list.Remove(this);
            All.Remove(this);

            if (list.Count < 1)
            {
                Lookup.Remove(identity.netId.Value);
            }
        }
    }

    public abstract void OnDrawGizmosSelected();
#if UNITY_EDITOR
    private void OnValidate()
    {
        if (identity != null)
        {
            return;
        }
        identity = GetComponent<NetworkIdentity>();
        if (identity == null)
        {
            identity = gameObject.AddComponent<NetworkIdentity>();
        }
    }
    private void Reset()
    {
        OnValidate();
    }
#endif
}