﻿using System.Collections.Generic;
using UnityEngine;

public static class AuthoritativePhysics
{
    private static readonly List<AuthoritativeWorldObject> WorldObjects = AuthoritativeWorldObject.All;
    private static readonly Dictionary<AuthoritativeWorldObject, ITimelineEvent> CurrentHistoryStates = new Dictionary<AuthoritativeWorldObject, ITimelineEvent>();

    public static bool CurrentlyRolledBack { get; private set; }

    public static bool Raycast(int time, Ray ray, out RaycastHit hit, float maxDistance, LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        var rolledBack = TryRollBack(time);
        var didHit = Physics.Raycast(ray, out hit, maxDistance, layerMask, queryTriggerInteraction);

        if (rolledBack)
        {
            Resume();
        }

        return didHit;
    }

    public static int RaycastAll(int time, Ray ray, RaycastHit[] hits, float maxDistance, LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        var rolledBack = TryRollBack(time);
        var count = Physics.RaycastNonAlloc(ray, hits, maxDistance, layerMask, queryTriggerInteraction);

        if (rolledBack)
        {
            Resume();
        }

        return count;
    }

    public static bool Linecast(int time, Vector3 start, Vector3 end, out RaycastHit hit, LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        var rolledBack = TryRollBack(time);
        var didHit = Physics.Linecast(start, end, out hit, layerMask, queryTriggerInteraction);

        if (rolledBack)
        {
            Resume();
        }

        return didHit;
    }

    public static int OverlapSphere(int time, Vector3 position, float radius, Collider[] results, LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        var rolledBack = TryRollBack(time);
        var count = Physics.OverlapSphereNonAlloc(position, radius, results, layerMask, queryTriggerInteraction);

        if (rolledBack)
        {
            Resume();
        }

        return count;
    }

    public static int OverlapCapsule(int time, Vector3 point0, Vector3 point1, float radius, Collider[] results, LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        var rolledBack = TryRollBack(time);
        var count = Physics.OverlapCapsuleNonAlloc(point0, point1, radius, results, layerMask, queryTriggerInteraction);

        if (rolledBack)
        {
            Resume();
        }

        return count;
    }

    public static int OverlapBox(int time, Vector3 center, Vector3 halfExtents, float radius, Collider[] results, Quaternion rotation, LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        var rolledBack = TryRollBack(time);
        var count = Physics.OverlapBoxNonAlloc(center, halfExtents, results, rotation, layerMask, queryTriggerInteraction);

        if (rolledBack)
        {
            Resume();
        }

        return count;
    }

    public static bool CapsuleCast(int time, Vector3 point0, Vector3 point1, float radius, Vector3 direction, out RaycastHit hit, float maxDistance, LayerMask layerMask,
        QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        var rolledBack = TryRollBack(time);
        var didHit = Physics.CapsuleCast(point0, point1, radius, direction, out hit, maxDistance, layerMask, queryTriggerInteraction);

        if (rolledBack)
        {
            Resume();
        }

        return didHit;
    }

    public static int CapsuleCastAll(int time, Vector3 point0, Vector3 point1, float radius, Vector3 direction, RaycastHit[] results, float maxDistance, LayerMask layerMask,
        QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        var rolledBack = TryRollBack(time);
        var count = Physics.CapsuleCastNonAlloc(point0, point1, radius, direction, results, maxDistance, layerMask, queryTriggerInteraction);

        if (rolledBack)
        {
            Resume();
        }

        return count;
    }

    public static bool SphereCast(int time, Ray ray, float radius, out RaycastHit hit, float maxDistance, LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        var rolledBack = TryRollBack(time);
        var didHit = Physics.SphereCast(ray, radius, out hit, maxDistance, layerMask, queryTriggerInteraction);

        if (rolledBack)
        {
            Resume();
        }

        return didHit;
    }

    public static int SphereCastAll(int time, Ray ray, float radius, RaycastHit[] results, float maxDistance, LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        var rolledBack = TryRollBack(time);
        var count = Physics.SphereCastNonAlloc(ray, radius, results, maxDistance, layerMask, queryTriggerInteraction);

        if (rolledBack)
        {
            Resume();
        }

        return count;
    }

    public static bool CheckSphere(int time, Vector3 position, float radius, LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        var rolledBack = TryRollBack(time);
        var didHit = Physics.CheckSphere(position, radius, layerMask, queryTriggerInteraction);

        if (rolledBack)
        {
            Resume();
        }

        return didHit;
    }

    public static bool CheckCapsule(int time, Vector3 start, Vector3 end, float radius, LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        var rolledBack = TryRollBack(time);
        var didHit = Physics.CheckCapsule(start, end, radius, layerMask, queryTriggerInteraction);

        if (rolledBack)
        {
            Resume();
        }

        return didHit;
    }

    public static bool CheckBox(int time, Vector3 center, Vector3 halfExtents, Quaternion rotation, LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        var rolledBack = TryRollBack(time);
        var didHit = Physics.CheckBox(center, halfExtents, rotation, layerMask, queryTriggerInteraction);

        if (rolledBack)
        {
            Resume();
        }

        return didHit;
    }

    public static bool BoxCast(int time, Vector3 center, Vector3 halfExtents, Vector3 direction, Quaternion rotation, float maxDistance, LayerMask layerMask,
        QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        var rolledBack = TryRollBack(time);
        var didHit = Physics.BoxCast(center, halfExtents, direction, rotation, maxDistance, layerMask, queryTriggerInteraction);

        if (rolledBack)
        {
            Resume();
        }

        return didHit;
    }

    public static int BoxCastAll(int time, Vector3 center, Vector3 halfExtents, Vector3 direction, RaycastHit[] results, Quaternion rotation, float maxDistance, LayerMask layerMask,
        QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        var rolledBack = TryRollBack(time);
        var count = Physics.BoxCastNonAlloc(center, halfExtents, direction, results, rotation, maxDistance, layerMask, queryTriggerInteraction);

        if (rolledBack)
        {
            Resume();
        }

        return count;
    }

    #region StateManagement

    public static bool TryRollBack(int time)
    {
        if (CurrentlyRolledBack || NetworkTime.Current <= time)
        {
            return false;
        }

        RollBack(time);
        return true;
    }

    public static void RollBack(int time)
    {
        if (CurrentlyRolledBack)
        {
            return;
        }

        for (var i = 0; i < WorldObjects.Count; ++i)
        {
            var obj = WorldObjects[i];

            if (obj == null)
            {
                continue;
            }

            CurrentHistoryStates[obj] = obj.ToEvent();
            obj.Interpolate.ApplyState(time);
        }

        CurrentlyRolledBack = true;
    }

    public static void Resume()
    {
        if (!CurrentlyRolledBack)
        {
            return;
        }

        for (var i = 0; i < WorldObjects.Count; ++i)
        {
            var obj = WorldObjects[i];

            if (obj == null)
            {
                continue;
            }

            ITimelineEvent evnt;

            if (CurrentHistoryStates.TryGetValue(obj, out evnt))
            {
                obj.ApplyEvent(evnt);
            }
        }

        CurrentHistoryStates.Clear();
        CurrentlyRolledBack = false;
    }

    #endregion
}