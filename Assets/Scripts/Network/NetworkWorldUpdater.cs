﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[NetworkSettings(channel = 1)]
public class NetworkWorldUpdater : NetworkBehaviour
{
    private static readonly List<AuthoritativeWorldObject> WorldObjects = AuthoritativeWorldObject.All;
    private static readonly Dictionary<uint, List<AuthoritativeWorldObject>> WorldObjectsLookup = AuthoritativeWorldObject.Lookup;
    private static readonly HashSet<AuthoritativeWorldObject> ReceivedObjects = new HashSet<AuthoritativeWorldObject>();
    private static readonly List<Action<float>> NetworkUpdateSubscriptions = new List<Action<float>>();

    public const int MaxHistoryMilliseconds = 10 * 1000;
    public static int InterpolateMilliseconds = 100;
    public static float FixedTimeStep => fixedUpdater.FixedTimeStep;

    [SerializeField] private int worldFrameRate = 20;
    [SerializeField] private bool autoInterpolate = true;
    [SerializeField] private int overrideInterpolateMilliseconds = 100;
    private readonly ByteWriter writer = new ByteWriter();
    private readonly ByteReader reader = new ByteReader();

    private static FixedUpdater fixedUpdater;
    private bool updatedThisFrame;

    public override float GetNetworkSendInterval()
    {
        return 1f / worldFrameRate;
    }

    public static void Subscribe(Action<float> callback)
    {
        NetworkUpdateSubscriptions.Add(callback);
    }

    public static void Unsubscribe(Action<float> callback)
    {
        NetworkUpdateSubscriptions.Remove(callback);
    }

    private void Awake()
    {
        InterpolateMilliseconds = autoInterpolate ? (int)(1f / worldFrameRate * 2f * 1000f * 2f) : overrideInterpolateMilliseconds;
        fixedUpdater = new FixedUpdater(OnNetworkUpdate, NetworkTime.GameTime, 1f / worldFrameRate);
    }

    public void Update()
    {
        if (!isServer)
        {
            Interpolate();
        }

        fixedUpdater.Update(NetworkTime.GameTime);
    }

    public void OnNetworkUpdate(float deltaTime)
    {
        updatedThisFrame = true;

        for (var i = NetworkUpdateSubscriptions.Count - 1; i >= 0; i--)
        {
            var update = NetworkUpdateSubscriptions[i];
            update?.Invoke(deltaTime);
        }
    }

    private static void Interpolate()
    {
        var time = NetworkTime.Current;
        var count = (uint)WorldObjects.Count;

        for (var i = 0; i < count; ++i)
        {
            var authoritative = WorldObjects[i];

            if (authoritative == null)
            {
                continue;
            }

            authoritative.DoInterpolate(time);
        }
    }

    private void LateUpdate()
    {
        if (!updatedThisFrame || !NetworkServer.active)
        {
            return;
        }

        updatedThisFrame = false;

        if (!isServer)
        {
            return;
        }

        var count = 0u;

        writer.Clear();
        writer.Write(NetworkTime.Current);

        foreach (var kvp in WorldObjectsLookup)
        {
            var id = kvp.Key;
            var list = kvp.Value;
            var idle = true;

            for (var i = 0; i < list.Count; ++i)
            {
                var authoritative = list[i];
                var current = authoritative.ToEvent();
                authoritative.AddEvent(NetworkTime.Current, current);

                if (!authoritative.IsIdle())
                {
                    idle = false;
                    break;
                }
            }

            if (idle)
            {
                continue;
            }

            writer.WritePackedUInt32(id);

            for (var i = 0; i < list.Count; ++i)
            {
                var authoritative = list[i];
                authoritative.Write(writer);
            }

            ++count;
        }

        RpcWorldUpdate(count, writer.ToArray());
    }

    [ClientRpc(channel = 1)]
    private void RpcWorldUpdate(uint count, byte[] bytes)
    {
        if (isServer)
        {
            return;
        }

        ReceivedObjects.Clear();
        reader.Replace(bytes);
        var rawTimestamp = reader.ReadInt();
        var interpolateTime = rawTimestamp + InterpolateMilliseconds;

        for (var i = 0; i < count; ++i)
        {
            var id = reader.ReadPackedUInt32();
            var objectsById = AuthoritativeWorldObject.Get(id);

            for (var j = 0; j < objectsById.Count; ++j)
            {
                var authoritative = objectsById[j];

                if (authoritative == null)
                {
                    Debug.LogError($"Unable to find AuthoritativeWorldObjects with id {id}. Aborting world update!");
                    break;
                }

                var timelineEvent = authoritative.Read(reader);
                authoritative.AddEvent(interpolateTime, timelineEvent);
                ReceivedObjects.Add(authoritative);
            }
        }

        for (var i = 0; i < WorldObjects.Count; ++i)
        {
            var authoritative = WorldObjects[i];

            if (authoritative == null)
            {
                continue;
            }

            if (ReceivedObjects.Contains(authoritative))
            {
                continue;
            }

            var lastEvent = authoritative.Timeline.LastEvent;
            authoritative.AddEvent(interpolateTime, lastEvent);
        }
    }

#if UNITY_EDITOR

    private void OnDrawGizmosSelected()
    {
        var count = (uint)WorldObjects.Count;

        for (var i = 0; i < count; ++i)
        {
            var authoritative = WorldObjects[i];

            if (authoritative == null)
            {
                continue;
            }

            authoritative.OnDrawGizmosSelected();
        }
    }

#endif
}