﻿using UnityEngine;

public class AuthoritativeTransformWithRotator : AuthoritativeWorldObject
{
    private bool isIdle;
    private Rotator rotator;

    public override bool IsIdle()
    {
        return isIdle;
    }

    public override void ApplyEvent(ITimelineEvent timelineEvent)
    {
        OnApplyState((TransformEvent)timelineEvent);
    }

    public override void AddEvent(int time, ITimelineEvent timelineEvent)
    {
        var previousEvent = (TransformEvent)Timeline.LastEvent;
        var current = (TransformEvent)timelineEvent;
        isIdle = previousEvent.IsIdleFrom(current);

        Timeline.Trim(NetworkTime.Current - NetworkWorldUpdater.MaxHistoryMilliseconds);
        Timeline.Add(time, timelineEvent);
    }

    public override ITimelineEvent ToEvent()
    {
        return new TransformEvent(transform.position, rotator.X, rotator.Y);
    }

    public override void Write(ByteWriter writer)
    {
        writer.WriteCompressedAngles(new Vector2(rotator.X, rotator.Y));
        writer.Write(transform.position);
    }

    public override ITimelineEvent Read(ByteReader reader)
    {
        var xy = reader.ReadCompressedVector2Angles();
        var position = reader.ReadVector3();
        return new TransformEvent(position, xy.x, xy.y);
    }

    protected override IHistoryEventTimeline CreateTimeline()
    {
        return new HistoryEventTimeline<TransformEvent>();
    }

    protected override IHistoryEventInterpolate CreateInterpolate(IHistoryEventTimeline timeline)
    {
        return new HistoryEventInterpolate<TransformEvent>((HistoryEventTimeline<TransformEvent>)timeline, OnApplyState);
    }

    private void Awake()
    {
        rotator = GetComponent<Rotator>();
    }

    private void OnApplyState(TransformEvent transformEvent)
    {
        transform.position = transformEvent.Position;
        rotator.ApplyState(new Rotator.RotatorState(transformEvent.X, transformEvent.Y));
    }

    public override void OnDrawGizmosSelected()
    {
        if (Timeline == null)
        {
            return;
        }

        var timeline = (HistoryEventTimeline<TransformEvent>)Timeline;
        var count = timeline.Channel.Entries.Count;

        for (var i = 0; i < count; ++i)
        {
            var height = Vector3.up * (i / (float)count);
            var state = timeline.Channel.Entries[i].State;
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(state.Position + height, 0.2f);

            if (i != 0)
            {
                var previous = timeline.Channel.Entries[i - 1];
                var previousHeight = Vector3.up * ((i - 1) / (float)count);

                Gizmos.color = Color.white;
                Gizmos.DrawLine(previous.State.Position + previousHeight, state.Position + height);
            }
        }

        var history = (TransformEvent)ToEvent();
        if (timeline.Channel.GetState(NetworkTime.Current - NetworkWorldUpdater.InterpolateMilliseconds, ref history))
        {
            DebugExtensions.DrawCapsule(history.Position, 2f, new Color(1f, 0.5f, 0f), 0.5f, 1);
        }

        if (count <= 1)
        {
            return;
        }

        if (!timeline.Channel.GetState(NetworkTime.Current + NetworkWorldUpdater.InterpolateMilliseconds, ref history))
        {
            return;
        }

        DebugExtensions.DrawCapsule(history.Position, 2f, Color.green, 0.5f, 1);
    }

    public struct TransformEvent : ITimelineEvent<TransformEvent>
    {
        private const float IdleEpsilon = 0.001f;

        public TransformEvent(Vector3 position, float x, float y)
        {
            Position = position;
            X = x;
            Y = y;
        }

        public readonly Vector3 Position;
        public readonly float X;
        public readonly float Y;

        public bool IsIdleFrom(TransformEvent otherEvent)
        {
            return (otherEvent.Position - Position).sqrMagnitude <= IdleEpsilon && Mathf.DeltaAngle(otherEvent.X, X) <= IdleEpsilon && Mathf.DeltaAngle(otherEvent.Y, Y) <= IdleEpsilon;
        }

        public TransformEvent InterpolateTo(TransformEvent otherEvent, float lerp)
        {
            return new TransformEvent(Vector3.Lerp(Position, otherEvent.Position, lerp), Mathf.LerpAngle(X, otherEvent.X, lerp), Mathf.LerpAngle(Y, otherEvent.Y, lerp));
        }

        public ITimelineEvent InterpolateTo(ITimelineEvent otherEvent, float lerp)
        {
            return InterpolateTo((TransformEvent)otherEvent, lerp);
        }

        public ITimelineEvent ExtrapolateFrom(IHistoryEventTimeline timeline, float secondsSinceLastUpdate)
        {
            var context = (HistoryEventTimeline<TransformEvent>)timeline;
            var velocity = Vector3.zero;
            var startIndex = Mathf.Max(timeline.EventCount - 5, 0);
            var previousState = context.Channel.Entries[startIndex];

            for (var i = startIndex + 1; i < timeline.EventCount; ++i)
            {
                var current = context.Channel.Entries[i];
                var stepVelocity = (current.State.Position - previousState.State.Position) / NetworkWorldUpdater.FixedTimeStep;
                velocity = (velocity + stepVelocity) / 2f;
                previousState = current;
            }

            var extrapolateDistance = velocity * secondsSinceLastUpdate;
            return new TransformEvent(Position + extrapolateDistance, X, Y);
        }
    }
}