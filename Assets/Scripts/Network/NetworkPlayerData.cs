﻿using UnityEngine.Networking;

public class NetworkPlayerData : NetworkBehaviour
{
    public string PlayerName { get; private set; } = "Unknown Name";

    public void SetName(string playerName)
    {
        PlayerName = playerName;
    }

    public override void OnStartServer()
    {
        UpdateNamePlate();
    }

    public override void OnStartClient()
    {
        UpdateNamePlate();
    }

    private void UpdateNamePlate()
    {
        var namePlate = GetComponent<UINamePlate>();
        
        if (namePlate != null)
        {
            namePlate.SetName(PlayerName);
        }
    }

    public override bool OnSerialize(NetworkWriter writer, bool initialState)
    {
        if (initialState)
        {
            writer.Write(PlayerName);
        }

        return true;
    }

    public override void OnDeserialize(NetworkReader reader, bool initialState)
    {
        if (initialState)
        {
            PlayerName = reader.ReadString();
        }
    }
}