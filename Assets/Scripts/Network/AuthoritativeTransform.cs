﻿using UnityEngine;

public class AuthoritativeTransform : AuthoritativeWorldObject
{
    private bool isIdle;

    public override bool IsIdle()
    {
        return isIdle;
    }

    public override void ApplyEvent(ITimelineEvent timelineEvent)
    {
        OnApplyState((TransformEvent)timelineEvent);
    }

    public override void AddEvent(int time, ITimelineEvent timelineEvent)
    {
        var previousEvent = (TransformEvent)Timeline.LastEvent;
        var current = (TransformEvent)timelineEvent;
        isIdle = previousEvent.IsIdleFrom(current);

        Timeline.Trim(NetworkTime.Current - NetworkWorldUpdater.MaxHistoryMilliseconds);
        Timeline.Add(time, timelineEvent);
    }

    public override ITimelineEvent ToEvent()
    {
        return new TransformEvent(transform.position, transform.rotation);
    }

    public override void Write(ByteWriter writer)
    {
        writer.Write(transform.position);
        writer.WriteCompressed(transform.rotation);
    }

    public override ITimelineEvent Read(ByteReader reader)
    {
        var position = reader.ReadVector3();
        var rotation = reader.ReadCompressedRotation();
        return new TransformEvent(position, rotation);
    }

    protected override IHistoryEventTimeline CreateTimeline()
    {
        return new HistoryEventTimeline<TransformEvent>();
    }

    protected override IHistoryEventInterpolate CreateInterpolate(IHistoryEventTimeline timeline)
    {
        return new HistoryEventInterpolate<TransformEvent>((HistoryEventTimeline<TransformEvent>)timeline, OnApplyState);
    }

    private void OnApplyState(TransformEvent transformEvent)
    {
        transform.SetPositionAndRotation(transformEvent.Position, transformEvent.Rotation);
    }

    public override void OnDrawGizmosSelected()
    {
        if (Timeline == null)
        {
            return;
        }

        var timeline = (HistoryEventTimeline<TransformEvent>)Timeline;

        var count = timeline.Channel.Entries.Count;

        for (var i = 0; i < count; ++i)
        {
            var height = Vector3.up * (i / (float)count);
            var state = timeline.Channel.Entries[i].State;
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(state.Position + height, 0.2f);

            if (i != 0)
            {
                var previous = timeline.Channel.Entries[i - 1];
                var previousHeight = Vector3.up * ((i - 1) / (float)count);

                Gizmos.color = Color.white;
                Gizmos.DrawLine(previous.State.Position + previousHeight, state.Position + height);
            }
        }

        var history = (TransformEvent)ToEvent();
        if (timeline.Channel.GetState(NetworkTime.Current - NetworkWorldUpdater.InterpolateMilliseconds, ref history))
        {
            DebugExtensions.DrawCapsule(transform.position, 2f, new Color(1f, 0.5f, 0f), 0.5f, 1);
        }

        if (count <= 1)
        {
            return;
        }

        if (!timeline.Channel.GetState(NetworkTime.Current + NetworkWorldUpdater.InterpolateMilliseconds, ref history))
        {
            return;
        }

        DebugExtensions.DrawCapsule(history.Position, 2f, Color.green, 0.5f, 1);
    }

    public struct TransformEvent : ITimelineEvent<TransformEvent>
    {
        private const float IdleEpsilon = 0.001f;

        public TransformEvent(Vector3 position, Quaternion rotation)
        {
            Position = position;
            Rotation = rotation;
        }

        public readonly Vector3 Position;
        public readonly Quaternion Rotation;

        public bool IsIdleFrom(TransformEvent otherEvent)
        {
            return (otherEvent.Position - Position).sqrMagnitude <= IdleEpsilon && Quaternion.Angle(otherEvent.Rotation, Rotation) <= IdleEpsilon;
        }

        public TransformEvent InterpolateTo(TransformEvent otherEvent, float lerp)
        {
            return new TransformEvent(Vector3.Lerp(Position, otherEvent.Position, lerp), Quaternion.Lerp(Rotation, otherEvent.Rotation, lerp));
        }

        public ITimelineEvent InterpolateTo(ITimelineEvent otherEvent, float lerp)
        {
            return InterpolateTo((TransformEvent)otherEvent, lerp);
        }

        public ITimelineEvent ExtrapolateFrom(IHistoryEventTimeline timeline, float secondsSinceLastUpdate)
        {
            var context = (HistoryEventTimeline<TransformEvent>)timeline;
            var velocity = Vector3.zero;
            var startIndex = Mathf.Max(timeline.EventCount - 5, 0);
            var previousState = context.Channel.Entries[startIndex];

            for (var i = startIndex + 1; i < timeline.EventCount; ++i)
            {
                var current = context.Channel.Entries[i];
                var stepVelocity = (current.State.Position - previousState.State.Position) / NetworkWorldUpdater.FixedTimeStep;
                velocity = (velocity + stepVelocity) / 2f;
                previousState = current;
            }

            var extrapolateDistance = velocity * secondsSinceLastUpdate;
            return new TransformEvent(Position + extrapolateDistance, Rotation);
        }
    }
}