﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[NetworkSettings(channel = 1, sendInterval = 0.1f)]
public class AuthoritativePlayer : NetworkBehaviour
{
    private static ConnectionConfig Config => NetworkManager.singleton.connectionConfig;
    public static event Action<AuthoritativePlayer> LocalPlayerStartEvent = delegate { };
    public static AuthoritativePlayer Local { get; private set; }

    [SerializeField] private bool clientSidePredict = false, autoWalk = false;
    [SerializeField] private int inputSampleFrameRate = 33;
    private Vector2 mouseInput;
    private KeyInput keyInput;
    private FixedUpdater fixedUpdater;
    private INeedNetworkInput[] networkInputTargets;
    private IAmAuthoritative[] authoritatives;
    private int lastUpdateTime;
    private readonly AuthoritativeStates authoritativeStates = new AuthoritativeStates();
    private readonly List<InputState> inputBuffer = new List<InputState>();

    public override void OnStartAuthority()
    {
        Local = this;
        LocalPlayerStartEvent(this);
    }

    private void Start()
    {
        Initialize();
        fixedUpdater = new FixedUpdater(OnInputUpdate, NetworkTime.GameTime, 1f / inputSampleFrameRate);
    }

    private void Initialize()
    {
        if (networkInputTargets == null)
        {
            networkInputTargets = GetComponents<INeedNetworkInput>();
        }

        if (authoritatives == null)
        {
            authoritatives = GetComponents<IAmAuthoritative>();

            foreach (var authoritative in authoritatives)
            {
                authoritativeStates.Add(authoritative);
            }

            authoritativeStates.GetLatest();
        }
    }

    private void OnDestroy()
    {
        foreach (var authoritative in authoritatives)
        {
            authoritativeStates.Remove(authoritative);
        }
    }

    private void Update()
    {
        if (hasAuthority)
        {
            if (Input.GetKeyDown(KeyCode.Equals))
            {
                autoWalk = !autoWalk;
            }

            if (autoWalk)
            {
                mouseInput = new Vector2(1.5f, 0f);
                keyInput = KeyInput.Move_Forward;
            }
            else
            {
                var frameMouse = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
                var frameKeys = KeyInputExtensions.GetCurrent();
                mouseInput += frameMouse;
                keyInput |= frameKeys;
            }
        }

        fixedUpdater.Update(NetworkTime.GameTime);
    }

    private void OnInputUpdate(float deltaTime)
    {
        if (isServer)
        {
            ServerUpdate(deltaTime);
        }

        if (hasAuthority)
        {
            AuthorityUpdate(deltaTime);
        }

        if (!isServer && !hasAuthority)
        {
            SimulateInput(Vector2.zero, keyInput, deltaTime, false);
        }
    }

    private void ServerUpdate(float deltaTime)
    {
        if (inputBuffer.Count == 0)
        {
            return;
        }

        var sendCorrection = false;

        for (var i = 0; i < inputBuffer.Count; ++i)
        {
            var input = inputBuffer[i];

            SimulateInput(input.MouseInput, input.KeyInput, deltaTime, true);

            if (sendCorrection)
            {
                continue;
            }

            var hash = authoritativeStates.GetLatest();

            if (inputBuffer[i].Hash != hash)
            {
                sendCorrection = true;
            }
        }

        if (sendCorrection)
        {
            var bytes = authoritativeStates.SerializeLatest();
            TargetCorrection(connectionToClient, inputBuffer[inputBuffer.Count - 1].Time, bytes);
        }
        else
        {
            TargetValid(connectionToClient, inputBuffer[inputBuffer.Count - 1].Time);
        }

        inputBuffer.Clear();
    }

    private void AuthorityUpdate(float deltaTime)
    {
        var hash = 0;
        var time = NetworkTime.Current;

        if (clientSidePredict)
        {
            SimulateInput(mouseInput, keyInput, deltaTime, true);
            hash = authoritativeStates.GetLatest();
            inputBuffer.Add(new InputState(time, mouseInput, keyInput, hash));
        }

        CmdReceiveInput(time, mouseInput, (uint)keyInput, hash);
        keyInput = KeyInput.None;
        mouseInput = Vector2.zero;
    }

    private void SimulateInput(Vector2 mouse, KeyInput keys, float deltaTime, bool isOwner)
    {
        for (var i = 0; i < networkInputTargets.Length; ++i)
        {
            var input = networkInputTargets[i];

            if (!input.Enabled || (input.OnlyOwnerUpdate && !isOwner))
            {
                continue;
            }

            input.OnInput(mouse, keys, deltaTime);
        }
    }

    [Command(channel = 1)]
    private void CmdReceiveInput(int time, Vector2 mouse, uint rawKeys, int hash)
    {
        try
        {
            if (NetworkTime.Current + Config.DisconnectTimeout < time)
            {
                Debug.LogError("Detected speed hack!");
                connectionToClient.Disconnect();
                return;
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            return;
        }

        if (time <= lastUpdateTime)
        {
            return;
        }

        var newKeyInput = (KeyInput)rawKeys;

        if (keyInput != newKeyInput)
        {
            keyInput = newKeyInput;
            RpcSetKeys((uint)keyInput);
        }

        inputBuffer.Add(new InputState(time, mouse, keyInput, hash));
        lastUpdateTime = time;
    }

    [TargetRpc(channel = 1)]
    private void TargetCorrection(NetworkConnection connection, int time, byte[] bytes)
    {
        if (time <= lastUpdateTime)
        {
            return;
        }

        authoritativeStates.DeserializeLatest(bytes);
        inputBuffer.RemoveAll(x => x.Time <= time);

        for (var i = 0; i < inputBuffer.Count; ++i)
        {
            var input = inputBuffer[i];

            SimulateInput(input.MouseInput, input.KeyInput, fixedUpdater.FixedTimeStep, true);
        }

        Debug.Log("Correcting");

        lastUpdateTime = time;
    }

    [TargetRpc(channel = 1)]
    private void TargetValid(NetworkConnection connection, int time)
    {
        if (time <= lastUpdateTime)
        {
            return;
        }

        inputBuffer.RemoveAll(x => x.Time <= time);
        lastUpdateTime = time;
    }

    [ClientRpc(channel = 1)]
    private void RpcSetKeys(uint rawKeys)
    {
        if (isServer || hasAuthority)
        {
            return;
        }

        keyInput = (KeyInput)rawKeys;
    }

    public override bool OnSerialize(NetworkWriter writer, bool initialState)
    {
        if (initialState)
        {
            Initialize();
            authoritativeStates.GetLatest();
            var bytes = authoritativeStates.SerializeLatest();
            writer.WriteBytesFull(bytes);
        }

        return true;
    }

    public override void OnDeserialize(NetworkReader reader, bool initialState)
    {
        if (initialState)
        {
            Initialize();
            var bytes = reader.ReadBytesAndSize();
            authoritativeStates.DeserializeLatest(bytes);
        }
    }

    private struct InputState
    {
        public InputState(int time, Vector2 mouseInput, KeyInput keyInput, int hash)
        {
            Time = time;
            MouseInput = mouseInput;
            KeyInput = keyInput;
            Hash = hash;
        }

        public readonly int Time;
        public readonly Vector2 MouseInput;
        public readonly KeyInput KeyInput;
        public readonly int Hash;
    }
}