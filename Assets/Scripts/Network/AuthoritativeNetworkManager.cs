﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class AuthoritativeNetworkManager : NetworkManager
{
    private static AuthoritativeNetworkManager ins;

    public static AuthoritativeNetworkManager Ins
    {
        get
        {
            if (ins == null)
            {
                ins = FindObjectOfType<AuthoritativeNetworkManager>();
            }

            return ins;
        }
    }

    public static event Action StartServerEvent = delegate { };
    public static event Action ClientConnectEvent = delegate { };

    public override void OnStartServer()
    {
        base.OnStartServer();
        Timed.NextFrame(() => StartServerEvent());
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        OnServerAddPlayer(conn, playerControllerId, null);
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId, NetworkReader extraMessageReader)
    {
        string playerName;

        try
        {
            var nameMessage = extraMessageReader.ReadMessage<StringMessage>();
            playerName = nameMessage.value;
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            conn.Disconnect();
            return;
        }

        playerName = playerName.Substring(0, Mathf.Min(playerName.Length, 32));
        Debug.Log($"Player spawned with name {playerName}");

        var playerObject = Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
        var playerData = playerObject.GetComponent<NetworkPlayerData>();
        playerData.SetName(playerName);

        try
        {
            if (!NetworkServer.ReplacePlayerForConnection(conn, playerObject, playerControllerId))
            {
                Debug.LogError("Failed to add player to connection id " + conn.connectionId);

                if (playerObject != null)
                {
                    Destroy(playerObject);
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);

            if (playerObject != null)
            {
                Destroy(playerObject);
            }
        }
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        ClientConnectEvent();
    }

    public static GameObject LocateNetworkObject(uint id)
    {
        return !NetworkServer.active ? ClientScene.FindLocalObject(new NetworkInstanceId(id)) : NetworkServer.FindLocalObject(new NetworkInstanceId(id));
    }

    private void Awake()
    {
        ins = this;
    }
}