﻿public interface IAuthorityState
{
    int CalculateStateHash();
}