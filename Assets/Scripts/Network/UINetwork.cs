﻿using UnityEngine;
using UnityEngine.Networking;

public class UINetwork : MonoBehaviour
{
    private void OnGUI()
    {
        if (NetworkManager.singleton == null)
        {
            return;
        }

        if (NetworkManager.singleton.isNetworkActive)
        {
            GUILayout.Label($"GameTime {NetworkTime.GameTime}");
            GUILayout.Label($"NetTime {NetworkTime.Current}");

            var ping = 0;
            var client = NetworkManager.singleton.client;

            if (!NetworkServer.active && client?.connection != null)
            {
                byte error;
                ping = NetworkTransport.GetCurrentRTT(0, client.connection.connectionId, out error);
            }

            GUILayout.Label($"Ping {ping}");
        }
    }
}