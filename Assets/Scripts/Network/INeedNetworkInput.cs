﻿using UnityEngine;

public interface INeedNetworkInput
{
    bool Enabled { get; }
    bool OnlyOwnerUpdate { get; }

    void OnInput(Vector2 mouseInput, KeyInput keyInput, float deltaTime);
}