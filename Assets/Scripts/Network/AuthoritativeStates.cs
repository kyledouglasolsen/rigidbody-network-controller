﻿using System.Collections.Generic;

public class AuthoritativeStates
{
    private readonly ByteWriter writer = new ByteWriter();
    private readonly ByteReader reader = new ByteReader();
    private readonly List<AuthoritativeChannel> channels = new List<AuthoritativeChannel>();

    public void Add(IAmAuthoritative authoritative)
    {
        channels.Add(new AuthoritativeChannel(authoritative));
    }

    public void Remove(IAmAuthoritative authoritative)
    {
        channels.RemoveAll(x => x.Authoritative == authoritative);
    }

    public void RollBackToLatest()
    {
        for (var i = 0; i < channels.Count; ++i)
        {
            channels[i].RollBackToLatest();
        }
    }

    public int GetLatest()
    {
        var hash = 0;

        for (var i = 0; i < channels.Count; ++i)
        {
            hash ^= channels[i].GetLatest();
        }

        return hash;
    }

    public byte[] SerializeLatest()
    {
        writer.Clear();

        for (var i = 0; i < channels.Count; ++i)
        {
            channels[i].SerializeLatest(writer);
        }

        return writer.ToArray();
    }

    public void DeserializeLatest(byte[] bytes)
    {
        reader.Replace(bytes);

        for (var i = 0; i < channels.Count; ++i)
        {
            channels[i].DeserializeLatest(reader);
        }
    }

    private class AuthoritativeChannel
    {
        public readonly IAmAuthoritative Authoritative;

        public IAuthorityState State { get; private set; }

        public AuthoritativeChannel(IAmAuthoritative authoritative)
        {
            Authoritative = authoritative;
        }

        public int GetLatest()
        {
            State = Authoritative.GetState();
            return State.CalculateStateHash();
        }

        public void SerializeLatest(ByteWriter writer)
        {
            Authoritative.SerializeState(writer);
        }

        public void DeserializeLatest(ByteReader reader)
        {
            var state = Authoritative.DeserializeState(reader);
            Authoritative.ApplyState(state);
        }

        public void RollBackToLatest()
        {
            Authoritative.ApplyState(State);
        }
    }
}