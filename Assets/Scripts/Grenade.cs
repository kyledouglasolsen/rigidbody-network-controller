﻿using UnityEngine;
using UnityEngine.Networking;

public abstract class Grenade : NetworkBehaviour
{
    protected static readonly Collider[] Overlaps = new Collider[100];

    [SerializeField] private LayerMask layerMask = Physics.DefaultRaycastLayers;
    [SerializeField] private float detonateSeconds = 5f;
    [SerializeField] private float radius = 5f;
    [SerializeField] private ParticleSystem explodeParticles;

    public override async void OnStartServer()
    {
        var throwable = GetComponent<Throwable>();
        var owner = throwable != null ? throwable.Owner : null;
        var detonateTime = (int)(NetworkTime.Current + detonateSeconds * 1000f);
        var delaySeconds = (detonateTime - NetworkTime.Current) / 1000f;

        if (delaySeconds > 0f)
        {
            await Timed.In(delaySeconds, null);
        }

        var explodeTime = NetworkTime.Current - NetworkWorldUpdater.InterpolateMilliseconds;
        var count = AuthoritativePhysics.OverlapSphere(explodeTime, transform.position, radius, Overlaps, layerMask, QueryTriggerInteraction.Ignore);

        for (var i = 0; i < count; ++i)
        {
            if (Overlaps[i].transform.IsChildOf(transform) || transform.IsChildOf(Overlaps[i].transform))
            {
                continue;
            }

            var worldPoint = Overlaps[i].ClosestPointOnBounds(transform.position);
            var directionTo = worldPoint - transform.position;
            var distance = directionTo.magnitude;

            if (distance > radius)
            {
                continue;
            }

            var pct = radius > 0f ? 1f - distance / radius : 0f;
            OnHitCollider(owner, Overlaps[i], worldPoint, directionTo, pct);
        }

        NetworkServer.Destroy(gameObject);
    }

    public override void OnNetworkDestroy()
    {
        var particles = Instantiate(explodeParticles, transform.position, Quaternion.identity);
        Destroy(particles.gameObject, 1f);
    }

    protected abstract void OnHitCollider(AuthoritativePlayer owner, Collider hitCollider, Vector3 worldPoint, Vector3 directionTo, float distancePercent);
}