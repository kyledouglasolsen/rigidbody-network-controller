﻿using UnityEngine;

public class Throwable : MonoBehaviour
{
    private Rigidbody rBody;

    public AuthoritativePlayer Owner { get; private set; }

    public void Initialize(AuthoritativePlayer owner, Vector3 velocity)
    {
        if(rBody == null)
        {
            rBody = GetComponent<Rigidbody>();
        }
        
        Owner = owner;
        rBody.AddForce(velocity, ForceMode.Impulse);
    }
}