﻿using UnityEngine;

public class AlignCamera : MonoBehaviour
{
    [SerializeField] private Vector3 position;

    private void Awake()
    {
        AuthoritativePlayer.LocalPlayerStartEvent += OnLocalPlayerStartEvent;
    }

    private void OnDestroy()
    {
        AuthoritativePlayer.LocalPlayerStartEvent -= OnLocalPlayerStartEvent;
    }

    private void OnLocalPlayerStartEvent(AuthoritativePlayer authoritativePlayer)
    {
        var cameraTarget = authoritativePlayer.GetComponentInChildren<CameraTarget>();
        transform.SetParent(cameraTarget.transform);
        transform.localPosition = position;
    }
}