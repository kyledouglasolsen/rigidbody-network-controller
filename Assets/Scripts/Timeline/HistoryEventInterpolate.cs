﻿using System;

public class HistoryEventInterpolate<T> : IHistoryEventInterpolate where T : ITimelineEvent<T>, new()
{
    private HistoryEventTimeline<T> eventTimeline;
    private Action<T> handler;
    private T historyState;

    public T HistoryState => historyState;

    public HistoryEventInterpolate(HistoryEventTimeline<T> timeline, Action<T> eventHandler) : this(timeline, eventHandler, new T())
    {
    }

    public HistoryEventInterpolate(HistoryEventTimeline<T> timeline, Action<T> eventHandler, T historyState)
    {
        this.historyState = historyState;
        eventTimeline = timeline;
        handler = eventHandler;
    }

    public void ApplyState(int time, bool forceHandler = false)
    {
        if (eventTimeline.Channel.GetState(time, ref historyState) || forceHandler)
        {
            handler(historyState);
        }
    }
}