﻿public interface ITimelineEvent
{
    ITimelineEvent InterpolateTo(ITimelineEvent otherEvent, float lerp);
    ITimelineEvent ExtrapolateFrom(IHistoryEventTimeline timeline, float secondsSinceLastUpdate);
}

public interface ITimelineEvent<T> : ITimelineEvent
{
    T InterpolateTo(T otherEvent, float lerp);
}