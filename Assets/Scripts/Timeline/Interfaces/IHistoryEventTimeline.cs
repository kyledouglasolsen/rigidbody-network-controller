﻿public interface IHistoryEventTimeline
{
    ITimelineEvent LastEvent { get; }
    int EventCount { get; }

    void Add(int time, ITimelineEvent timelineEvent);
    void Trim(int time);
    void Clear();
}