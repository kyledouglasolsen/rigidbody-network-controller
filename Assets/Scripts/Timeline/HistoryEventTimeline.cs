﻿using System.Collections.Generic;
using UnityEngine;

public class HistoryEventTimeline<T> : IHistoryEventTimeline where T : ITimelineEvent<T>
{
    public HistoryTimelineChannel Channel { get; }

    public HistoryEventTimeline()
    {
        Channel = new HistoryTimelineChannel(this);
    }

    public ITimelineEvent LastEvent => Channel.LastState;
    public int EventCount => Channel.Entries.Count;

    public void Add(int time, T state)
    {
        Channel.Add(time, state);
    }

    public void Add(int time, ITimelineEvent timelineEvent)
    {
        if (!(timelineEvent is T))
        {
            Debug.LogError($"Unable to add state of type {timelineEvent.GetType().Name} to HistoryEventTimeline<{typeof(T).Name}>");
            return;
        }

        Add(time, (T)timelineEvent);
    }

    public void Trim(int time)
    {
        Channel.TrimBefore(time);
    }

    public void Clear()
    {
        Channel.Clear();
    }

    public class HistoryTimelineChannel
    {
        private readonly PooledQueue<TimelineEntry> entryPool = new PooledQueue<TimelineEntry>(CreateEntry);
        private readonly HistoryEventTimeline<T> timeline;

        public HistoryTimelineChannel(HistoryEventTimeline<T> timeline)
        {
            this.timeline = timeline;
            Entries = new List<TimelineEntry>(32);
            LastStateTime = int.MinValue;
        }

        public List<TimelineEntry> Entries { get; }
        public int LastStateTime { get; private set; }
        public T LastState { get; private set; }

        public void Add(int time, T state)
        {
            var entry = entryPool.Dequeue();
            entry.Time = time;
            entry.State = state;

            Entries.Add(entry);
            LastStateTime = time;
            LastState = state;
        }

        public bool GetState(int targetTime, ref T state)
        {
            if (Entries.Count < 1)
            {
                return false;
            }

            var last = Entries.Count - 1;

            if (LastStateTime > targetTime)
            {
                for (var i = last; i >= 0; --i)
                {
                    if (Entries[i].Time > targetTime)
                    {
                        continue;
                    }

                    var left = Entries[i];
                    var right = Entries[Mathf.Min(i + 1, last)];
                    var length = right.Time - left.Time;
                    var lerp = 0f;

                    if (length > 0)
                    {
                        lerp = (targetTime - left.Time) / (float)length;
                    }

                    state = left.State.InterpolateTo(right.State, lerp);
                    return true;
                }
            }
            else
            {
                var secondsSinceLastUpdate = (targetTime - LastStateTime) / 1000f;
                state = (T)Entries[last].State.ExtrapolateFrom(timeline, secondsSinceLastUpdate);
            }

            return true;
        }

        public void TrimBefore(int targetTime)
        {
            if (Entries.Count < 2)
            {
                return;
            }

            for (var i = Entries.Count - 1; i >= 0; --i)
            {
                if (Entries[i].Time > targetTime)
                {
                    continue;
                }

                var index = Mathf.Min(Entries.Count - 1, i);

                for (var j = 0; j < index; ++j)
                {
                    entryPool.Enqueue(Entries[j]);
                }

                Entries.RemoveRange(0, index);

                break;
            }
        }

        public void Clear()
        {
            for (var i = 0; i < Entries.Count; ++i)
            {
                entryPool.Enqueue(Entries[i]);
            }

            Entries.Clear();
        }

        private static TimelineEntry CreateEntry()
        {
            return new TimelineEntry(0, default(T));
        }
    }

    public class TimelineEntry
    {
        public TimelineEntry(int time, T state)
        {
            Time = time;
            State = state;
        }

        public int Time;
        public T State;

        public new string ToString()
        {
            return $"{Time} - {State}";
        }
    }
}