﻿using UnityEngine;
using UnityEngine.Networking;

public class StartServer : MonoBehaviour
{
    [SerializeField] private int frameRate = 0;
    private NetworkManager manager;

    private void Start()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = frameRate;

        manager = GetComponent<NetworkManager>();
        manager?.StartServer();
    }
}