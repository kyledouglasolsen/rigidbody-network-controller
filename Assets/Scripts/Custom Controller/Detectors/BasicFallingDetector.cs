﻿using System;
using UnityEngine;

public class BasicFallingDetector : MonoBehaviour, IFallingDetector
{
    private ICollisionDetector groundDetector;
    private bool isFallingInternal;
    private float lastEvaluateTime;

    [SerializeField] private float minFallThreshold = 0.5f;

    private float startFallHeight;
    public event Action<bool> OnFallingChangedEvent = delegate { };

    public float MinFallThreshold
    {
        get { return minFallThreshold; }
        set { minFallThreshold = value; }
    }

    public bool IsFalling { get; private set; }
    public float FallingSeconds { get; private set; }
    public float TotalFallDistance { get; private set; }

    public bool Evaluate(Vector3 velocity)
    {
        var grounded = (groundDetector == null) || groundDetector.IsGrounded;
        isFallingInternal = !grounded;

        if (isFallingInternal)
        {
            if (transform.position.y > startFallHeight)
                startFallHeight = transform.position.y;

            TotalFallDistance = startFallHeight - transform.position.y;

            if (!IsFalling && (TotalFallDistance > minFallThreshold))
            {
                IsFalling = true;
                startFallHeight = float.MinValue;
                OnFallingChangedEvent(IsFalling);
            }

            if (IsFalling)
                FallingSeconds += Time.time - lastEvaluateTime;
        }
        else
        {
            if (IsFalling)
            {
                IsFalling = false;
                OnFallingChangedEvent(IsFalling);
            }

            FallingSeconds = 0f;
            TotalFallDistance = 0f;
            startFallHeight = float.MinValue;
        }

        lastEvaluateTime = Time.time;

        return IsFalling;
    }

    private void Awake()
    {
        lastEvaluateTime = Time.time;
    }

    private void Start()
    {
        groundDetector = GetComponent<ICollisionDetector>();
    }
}