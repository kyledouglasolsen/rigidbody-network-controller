﻿using System;
using UnityEngine;

public class SphereCastCollisionDetector : MonoBehaviour, ICollisionDetector
{
    private static RaycastHit[] hits = new RaycastHit[10];

    [SerializeField] private float radius = 0.5f;
    [Range(0f, 1f)] [SerializeField] private float maxHeightPct = 0.5f;
    [SerializeField] private float groundedDistance = 0.08f;
    [SerializeField] private LayerMask layermask = Physics.DefaultRaycastLayers;
    private float lastEvaluateTime;

    public float Radius
    {
        get { return radius; }
        set { radius = value; }
    }

    public float MaxHeightPct
    {
        get { return maxHeightPct; }
        set { maxHeightPct = value; }
    }

    public LayerMask Layermask
    {
        get { return layermask; }
        set { layermask = value; }
    }

    public float GroundDistance
    {
        get { return groundedDistance; }
        set { groundedDistance = value; }
    }

    public bool IsGrounded { get; private set; }
    public RaycastHit GroundHit { get; private set; }
    public float UngroundedSeconds { get; private set; }

    public event Action<bool> OnGroundedChangedEvent = delegate { };

    public bool Evaluate()
    {
        var ray = new Ray(CastPosition(), -transform.up);
        var count = Physics.SphereCastNonAlloc(ray, radius, hits, groundedDistance, layermask, QueryTriggerInteraction.Ignore);
        var newGrounded = false;

        for (var i = 0; i < count; ++i)
        {
            if (hits[i].collider.transform.IsChildOf(transform))
            {
                continue;
            }

            var localHitPoint = transform.InverseTransformPoint(hits[i].point);
            if (localHitPoint.y > maxHeightPct * radius)
            {
                continue;
            }

            GroundHit = hits[i];
            newGrounded = true;
            break;
        }

        SetGrounded(newGrounded);

        if (!IsGrounded)
        {
            UngroundedSeconds += Time.time - lastEvaluateTime;
        }

        lastEvaluateTime = Time.time;

        if (count > hits.Length)
        {
            hits = new RaycastHit[count];
        }

        return IsGrounded;
    }

    public void SetGrounded(bool grounded)
    {
        if (IsGrounded != grounded)
        {
            IsGrounded = grounded;
            OnGroundedChangedEvent?.Invoke(IsGrounded);
        }

        if (IsGrounded)
        {
            UngroundedSeconds = 0f;
        }
    }

    private void Awake()
    {
        lastEvaluateTime = Time.time;
    }

    private Vector3 CastPosition()
    {
        var position = transform.parent != null ? transform.TransformPoint(transform.localPosition) : transform.localPosition;
        return position + transform.up * radius;
    }

    private void OnDrawGizmosSelected()
    {
        var offset = CastPosition() - transform.up * groundedDistance;
        var maxOffset = transform.position + transform.up * radius * maxHeightPct;
        DebugExtensions.DrawArrow(transform.position, -transform.up * groundedDistance);

        Gizmos.DrawLine(new Vector3(offset.x - radius, offset.y, offset.z), new Vector3(offset.x + radius, offset.y, offset.z));
        Gizmos.DrawLine(new Vector3(offset.x, offset.y, offset.z - radius), new Vector3(offset.x, offset.y, offset.z + radius));

        Gizmos.DrawWireSphere(offset, radius);

        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector3(maxOffset.x - radius, maxOffset.y, maxOffset.z), new Vector3(maxOffset.x + radius, maxOffset.y, maxOffset.z));
        Gizmos.DrawLine(new Vector3(maxOffset.x, maxOffset.y, maxOffset.z - radius), new Vector3(maxOffset.x, maxOffset.y, maxOffset.z + radius));
    }
}