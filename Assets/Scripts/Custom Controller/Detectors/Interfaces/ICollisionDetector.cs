﻿using System;
using UnityEngine;

public interface ICollisionDetector
{
    bool IsGrounded { get; }
    RaycastHit GroundHit { get; }
    float GroundDistance { get; }
    float UngroundedSeconds { get; }
    event Action<bool> OnGroundedChangedEvent;
    bool Evaluate();
    void SetGrounded(bool grounded);
}