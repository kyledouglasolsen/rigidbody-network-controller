﻿using System;
using UnityEngine;

public interface IFallingDetector
{
    float MinFallThreshold { get; set; }

    bool IsFalling { get; }
    float FallingSeconds { get; }
    float TotalFallDistance { get; }
    event Action<bool> OnFallingChangedEvent;

    bool Evaluate(Vector3 velocity);
}