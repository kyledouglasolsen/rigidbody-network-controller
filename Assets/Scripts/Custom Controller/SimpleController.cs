﻿using UnityEngine;

public class SimpleController : MonoBehaviour
{
    private static readonly Collider[] Overlaps = new Collider[100];

    [SerializeField] private LayerMask collisionMask = Physics.DefaultRaycastLayers;
    private ICollisionDetector collisionDetector;
    private CapsuleCollider col;

    public bool IsGrounded => collisionDetector.IsGrounded;

    private void Awake()
    {
        collisionDetector = GetComponent<ICollisionDetector>();
        col = GetComponent<CapsuleCollider>();
    }

    public void Move(Vector3 velocity)
    {
        if (!col.enabled)
        {
            return;
        }

        var newPosition = transform.position + velocity;
        var rotation = transform.rotation;

        var p1 = newPosition + col.center + Vector3.up * -col.height * 0.5F;
        var p2 = p1 + Vector3.up * col.height;
        var count = Physics.OverlapCapsuleNonAlloc(p1, p2, col.radius, Overlaps, collisionMask, QueryTriggerInteraction.Ignore);

        for (var i = 0; i < count; ++i)
        {
            if (col == Overlaps[i])
            {
                continue;
            }

            Vector3 direction;
            float distance;
            var overlapPosition = Overlaps[i].transform.position;
            var overlapRotation = Overlaps[i].transform.rotation;

            if (Physics.ComputePenetration(col, newPosition, rotation, Overlaps[i], overlapPosition, overlapRotation, out direction, out distance))
            {
                newPosition += direction * distance;
            }
        }

        transform.position = newPosition;
        collisionDetector.Evaluate();
    }

    public void SetGrounded(bool isGrounded)
    {
        collisionDetector.SetGrounded(isGrounded);
    }
}