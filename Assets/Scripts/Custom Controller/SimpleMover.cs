﻿using UnityEngine;

public class SimpleMover : MonoBehaviour
{
    [SerializeField] private float groundSpeed = 6f, airSpeed = 2f;
    [SerializeField] private float jumpSpeed = 8f;
    [SerializeField] private Vector3 gravity = Physics.gravity;
    private Vector3 velocity = Vector3.zero, airNudge;
    private bool nudged;
    private SimpleController controller;

    public Vector3 Velocity => velocity;

    private void Awake()
    {
        controller = GetComponent<SimpleController>();
    }

    private void Update()
    {
        var frameMouse = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        var frameKeys = KeyInputExtensions.GetCurrent();
        OnInput(frameMouse, frameKeys, Time.deltaTime);
    }

    public void OnInput(Vector2 mouseInput, KeyInput keyInput, float deltaTime)
    {
        if (controller.IsGrounded)
        {
            nudged = false;
            velocity = transform.TransformDirection(keyInput.AsInputVelocity()) * groundSpeed;

            if (keyInput.HasFlag(KeyInput.Move_Jump))
            {
                velocity.y = jumpSpeed;
            }
        }
        else if (!nudged)
        {
            var nudgeVelocity = transform.TransformDirection(keyInput.AsInputVelocity()) * airSpeed;

            if (nudgeVelocity != Vector3.zero)
            {
                velocity += nudgeVelocity;
                nudged = true;
            }
        }

        velocity += gravity * deltaTime;
        velocity = velocity.Round(3);
        var horizontal = new Vector3(velocity.x, 0f, velocity.z);
        var vertical = new Vector3(0f, velocity.y, 0f);

        controller.Move(horizontal * deltaTime);
        controller.Move(vertical * deltaTime);
        controller.transform.position = transform.position.Round(3);
    }
}