﻿using UnityEngine;

public class CollisionSurface : MonoBehaviour
{
    public enum Type
    {
        Concrete,
        Dirt,
        Flesh,
        Foliage,
        Glass,
        Metal,
        Paper,
        Plaster,
        Plastic,
        Water,
        Wood
    }

    [SerializeField] private Type surfaceType = Type.Concrete;

    public Type SurfaceType => surfaceType;

    public void SetSurfaceType(Type type)
    {
        surfaceType = type;
    }
}