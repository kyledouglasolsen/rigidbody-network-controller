﻿public interface ICareAboutDeath
{
    void OnDie();
    void OnRespawn();
}