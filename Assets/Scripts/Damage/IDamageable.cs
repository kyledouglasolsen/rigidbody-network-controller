﻿public interface IDamageable
{
    void DoDamage(DamageInfo info);
}