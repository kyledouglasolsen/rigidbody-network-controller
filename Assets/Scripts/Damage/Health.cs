﻿using System;
using System.Threading;
using UnityEngine;
using UnityEngine.Networking;

public class Health : NetworkBehaviour, IDamageable
{
    public static event Action<DamageInfo, Health> OnDealDamageEvent = delegate { };

    [SerializeField] private uint startingHealth = 10;
    [SerializeField] private float respawnSeconds = 5f;
    [SerializeField] private ParticleSystem bloodImpact;
    private ICareAboutDeath[] careAboutDeaths;
    private ByteWriter writer = new ByteWriter();
    private ByteReader reader = new ByteReader();
    private uint currentHealth;
    private SoftObjectDisable objectDisable;
    private CancellationTokenSource respawn;

    public event Action<DamageInfo> OnTakeDamageEvent = delegate { };
    public event Action OnDieEvent = delegate { };
    public event Action OnRespawnEvent = delegate { };

    public float RespawnSeconds => respawnSeconds;

    public void Respawn()
    {
        currentHealth = startingHealth;
        objectDisable.Enable();
        transform.position = Vector3.zero;

        OnRespawnEvent();

        for (var i = 0; i < careAboutDeaths.Length; ++i)
        {
            careAboutDeaths[i].OnRespawn();
        }

        if (NetworkServer.active)
        {
            RpcRespawn();
        }
    }

    public void DoDamage(DamageInfo info)
    {
        if (AuthoritativePlayer.Local == info.DamageBy)
        {
            SpawnBlood(info.LocalPosition);
        }

        if (NetworkServer.active)
        {
            var damageToDo = (uint)Mathf.Min(currentHealth, info.Damage);
            var newHealth = currentHealth - damageToDo;
            LocalSetHealth(newHealth);

            writer.Clear();
            writer.WritePackedUInt32(currentHealth);
            writer.Write(info);
            RpcImpact(writer.ToArray());
        }
    }

    [ClientRpc]
    private void RpcImpact(byte[] bytes)
    {
        reader.Replace(bytes);
        var newHealth = reader.ReadPackedUInt32();
        LocalSetHealth(newHealth);
        var info = reader.ReadDamageInfo();
        OnTakeDamageEvent(info);
        OnDealDamageEvent(info, this);

        SpawnBlood(info.LocalPosition);
    }

    [ClientRpc]
    private void RpcRespawn()
    {
        Respawn();
    }

    private void LocalSetHealth(uint newHealth)
    {
        currentHealth = newHealth;

        if (currentHealth == 0)
        {
            objectDisable.Disable();
            OnDieEvent();

            for (var i = 0; i < careAboutDeaths.Length; ++i)
            {
                careAboutDeaths[i].OnDie();
            }

            if (NetworkServer.active)
            {
                respawn?.Cancel();
                respawn = new CancellationTokenSource();
                Timed.In(respawnSeconds, Respawn, respawn);
            }
        }
    }

    private void SpawnBlood(Vector3 localPosition)
    {
        var blood = Instantiate(bloodImpact, transform);
        blood.transform.localPosition = localPosition;
        blood.transform.localRotation = Quaternion.identity;
        Destroy(blood.gameObject, 1f);
    }

    private void Awake()
    {
        careAboutDeaths = GetComponentsInChildren<ICareAboutDeath>(true);
        objectDisable = new SoftObjectDisable(gameObject);
        Respawn();
    }

    private void OnDisable()
    {
        respawn?.Cancel();
    }
}