﻿using UnityEngine;
using UnityEngine.Networking;

public struct DamageInfo
{
    public readonly AuthoritativePlayer DamageBy;
    public readonly uint Damage;
    public readonly Vector3 LocalPosition;

    public DamageInfo(AuthoritativePlayer damageBy, uint damage, Vector3 localPosition)
    {
        DamageBy = damageBy;
        Damage = damage;
        LocalPosition = localPosition;
    }
}

internal static class DamageInfoWriter
{
    public static void Write(this ByteWriter writer, DamageInfo info)
    {
        var identity = info.DamageBy != null ? info.DamageBy.GetComponent<NetworkIdentity>() : null;
        writer.WritePackedUInt32(identity != null ? identity.netId.Value : 0u);
        writer.WritePackedUInt32(info.Damage);
        writer.WriteCompressed(info.LocalPosition);
    }

    public static DamageInfo ReadDamageInfo(this ByteReader reader)
    {
        var id = new NetworkInstanceId(reader.ReadPackedUInt32());
        var damageByObject = NetworkServer.active ? NetworkServer.FindLocalObject(id) : ClientScene.FindLocalObject(id);
        return new DamageInfo(damageByObject != null ? damageByObject.GetComponent<AuthoritativePlayer>() : null, reader.ReadPackedUInt32(), reader.ReadCompressedVector3());
    }
}