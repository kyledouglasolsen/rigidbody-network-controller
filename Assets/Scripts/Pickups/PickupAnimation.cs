﻿using UnityEngine;

public class PickupAnimation : MonoBehaviour
{
    [SerializeField] private float bobHeight = 1f;
    [SerializeField] private float bobSpeed = 1f;
    [SerializeField] private float rotateSpeed = 1f;
    private float defaultY;

    private void Start()
    {
        defaultY = transform.position.y;
    }

    private void Update()
    {
        var pos = transform.position;
        transform.position = new Vector3(pos.x, defaultY + Mathf.Sin(Time.time * bobSpeed) * bobHeight, pos.z);
        transform.Rotate(0f, rotateSpeed, 0f, Space.World);
    }
}