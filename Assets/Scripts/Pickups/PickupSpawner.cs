﻿using UnityEngine;
using UnityEngine.Networking;

public class PickupSpawner : MonoBehaviour
{
    [SerializeField] private GameObject[] pickups;
    [SerializeField] private int spawnCount = 20;
    [SerializeField] private float width = 100f, length = 100f;

    private void Start()
    {
        if (pickups.Length < 1)
        {
            return;
        }

        AuthoritativeNetworkManager.StartServerEvent += () =>
        {
            var y = transform.position.y;

            for (var i = 0; i < spawnCount; ++i)
            {
                var position = new Vector3(Random.Range(-width, width), y, Random.Range(-length, length));
                var instance = Instantiate(pickups[Random.Range(0, pickups.Length)], position, Quaternion.identity);
                NetworkServer.Spawn(instance);
            }
        };
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(width * 2f, transform.position.y, length * 2f));
    }
}