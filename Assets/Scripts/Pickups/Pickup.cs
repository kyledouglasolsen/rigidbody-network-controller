﻿using System.Threading;
using UnityEngine;
using UnityEngine.Networking;

public abstract class Pickup : NetworkBehaviour
{
    [SerializeField] private float respawnSeconds = 30f;
    private SoftObjectDisable softDisable;
    private CancellationTokenSource respawn;

    [ServerCallback]
    protected void OnTriggerEnter(Collider other)
    {
        var authoritativePlayer = other.GetComponentInParent<AuthoritativePlayer>();

        if (authoritativePlayer == null)
        {
            return;
        }

        var respawnTime = NetworkTime.Current + (int)(respawnSeconds * 1000f);

        if (!NetworkClient.active)
        {
            OnPlayerPickedUp(authoritativePlayer, respawnTime);
            DisableAndRespawn(respawnTime);
        }

        RpcPlayerPickedUp(authoritativePlayer.gameObject, respawnTime);
    }

    protected abstract void OnPlayerPickedUp(AuthoritativePlayer player, int respawnNetworkTime);

    [ClientRpc]
    private void RpcPlayerPickedUp(GameObject playerGameObject, int respawnNetworkTime)
    {
        var authoritativePlayer = playerGameObject.GetComponentInParent<AuthoritativePlayer>();

        if (authoritativePlayer == null)
        {
            return;
        }

        OnPlayerPickedUp(authoritativePlayer, respawnNetworkTime);
        DisableAndRespawn(respawnNetworkTime);
    }

    private void DisableAndRespawn(int respawnNetworkTime)
    {
        var seconds = (respawnNetworkTime - NetworkTime.Current) / 1000f;

        if (seconds > 0f)
        {
            respawn?.Cancel();
            respawn = new CancellationTokenSource();
            softDisable.Disable();
            Timed.In(seconds, softDisable.Enable, respawn);
        }
    }

    private void Awake()
    {
        softDisable = new SoftObjectDisable(gameObject);
    }
}