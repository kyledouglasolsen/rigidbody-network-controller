﻿using UnityEngine;

public class SpeedPickup : Pickup
{
    [SerializeField] private float duration = 10f;
    [SerializeField] private float moveSpeedMultiplier = 2f;

    protected override void OnPlayerPickedUp(AuthoritativePlayer player, int respawnNetworkTime)
    {
        var buffManager = player.GetComponent<BuffManager>();
        
        if(buffManager == null)
        {
            return;
        }
        
        buffManager.Add(new MoveSpeedBuff(moveSpeedMultiplier, NetworkTime.Current + (int)(duration * 1000f)));
    }
}