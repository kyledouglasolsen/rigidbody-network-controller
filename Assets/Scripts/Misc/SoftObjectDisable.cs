﻿using System.Collections.Generic;
using UnityEngine;

public class SoftObjectDisable
{
    private readonly List<Collider> colliders = new List<Collider>();
    private readonly List<Renderer> renderers = new List<Renderer>();

    public readonly GameObject GameObject;

    public SoftObjectDisable(GameObject gameObject)
    {
        GameObject = gameObject;
    }

    public void SetEnabled(bool enable)
    {
        if (enable)
        {
            Enable();
        }
        else
        {
            Disable();
        }
    }

    public void Disable()
    {
        colliders.Clear();
        renderers.Clear();
        GameObject.GetComponentsInChildren(true, colliders);
        GameObject.GetComponentsInChildren(true, renderers);

        InternalEnable(false);
    }

    public void Enable()
    {
        InternalEnable(true);
    }

    private void InternalEnable(bool enable)
    {
        for (var i = 0; i < colliders.Count; ++i)
        {
            if (colliders[i] != null)
            {
                colliders[i].enabled = enable;
            }
        }

        for (var i = 0; i < renderers.Count; ++i)
        {
            if (renderers[i] != null)
            {
                renderers[i].enabled = enable;
            }
        }
    }
}