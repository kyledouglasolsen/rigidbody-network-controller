﻿using System;

public class FixedUpdater
{
    private float physicsTime;
    private Action<float> onFixedUpdate;

    public FixedUpdater(Action<float> onFixedUpdate, float currentTime, float fixedTimeStep)
    {
        physicsTime = currentTime;
        FixedTimeStep = fixedTimeStep;
        this.onFixedUpdate = onFixedUpdate;
    }

    public float FixedTimeStep { get; }

    public bool Update(float currentTime)
    {
        var ranUpdate = false;
        while (physicsTime < currentTime)
        {
            onFixedUpdate(FixedTimeStep);
            physicsTime += FixedTimeStep;
            ranUpdate = true;
        }
        return ranUpdate;
    }
}