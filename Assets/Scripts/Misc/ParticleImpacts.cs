﻿using System;
using UnityEngine;

public class ParticleImpacts : MonoBehaviour
{
    [SerializeField] private ImpactParticle[] impactParticles;

    public void TryCreateImpactParticle(RaycastHit hit)
    {
        if (impactParticles.Length < 1)
        {
            return;
        }

        var surface = hit.collider.GetComponentInParent<CollisionSurface>();
        TryCreateImpactParticle(surface, hit.point);
    }

    public void TryCreateImpactParticle(CollisionSurface surface, Vector3 position)
    {
        var surfaceType = surface == null ? CollisionSurface.Type.Concrete : surface.SurfaceType;

        for (var i = 0; i < impactParticles.Length; ++i)
        {
            if (impactParticles[i].SurfaceType != surfaceType)
            {
                continue;
            }

            impactParticles[i].Instantiate(position);
            return;
        }

        impactParticles[0].Instantiate(position);
    }

    [Serializable]
    private class ImpactParticle
    {
        [SerializeField] private CollisionSurface.Type surfaceType;
        [SerializeField] private ParticleSystem particle;

        public CollisionSurface.Type SurfaceType => surfaceType;

        public void Instantiate(Vector3 position)
        {
            var instance = UnityEngine.Object.Instantiate(particle, position, Quaternion.identity);
            Destroy(instance.gameObject, 1f);
        }
    }
}