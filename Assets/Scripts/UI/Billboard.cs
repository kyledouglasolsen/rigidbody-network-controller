﻿using UnityEngine;

public class Billboard : MonoBehaviour
{
    private Transform targetCameraTransform;

    private void Awake()
    {
        targetCameraTransform = Camera.main.transform;
    }

    private void LateUpdate()
    {
        transform.rotation = Quaternion.LookRotation(transform.position - targetCameraTransform.position);
    }
}