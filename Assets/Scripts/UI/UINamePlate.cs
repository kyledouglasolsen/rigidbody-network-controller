﻿using UnityEngine;
using UnityEngine.UI;

public class UINamePlate : MonoBehaviour
{
    [SerializeField] private Text nameText;

    public void SetName(string playerName)
    {
        nameText.text = playerName;
    }
}