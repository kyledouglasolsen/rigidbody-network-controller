﻿using UnityEngine;

public class UIImpact : MonoBehaviour
{
    [SerializeField] private AudioClip hitMarker = null;
    private CanvasGroup canvasGroup;

    private void Awake()
    {
        canvasGroup = GetComponentInChildren<CanvasGroup>();
        canvasGroup.alpha = 0f;
        enabled = false;

        Health.OnDealDamageEvent += OnDealDamage;
    }

    private void OnDestroy()
    {
        Health.OnDealDamageEvent -= OnDealDamage;
    }

    private void Update()
    {
        canvasGroup.alpha -= Time.deltaTime * 5f;

        if (canvasGroup.alpha <= 0f)
        {
            canvasGroup.alpha = 0f;
            enabled = false;
        }
    }

    private void OnDealDamage(DamageInfo info, Health health)
    {
        if (AuthoritativePlayer.Local != info.DamageBy)
        {
            return;
        }

        Debug.Log($"{info.DamageBy.name} hit {health.gameObject.name}");
        AudioSource.PlayClipAtPoint(hitMarker, info.DamageBy.transform.position, 0.5f);
        canvasGroup.alpha = 1f;
        enabled = true;
    }
}