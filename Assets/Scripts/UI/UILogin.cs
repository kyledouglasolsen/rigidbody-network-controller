﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.UI;

public class UILogin : MonoBehaviour
{
    public static string PlayerName { get; private set; } = "Unknown Name";

    [SerializeField] private InputField nameInput;
    [SerializeField] private Button loginButton;

    private void Awake()
    {
        PlayerName = PlayerPrefs.GetString("AuthoritativePlayerName", "Unknown Name");
        nameInput.text = PlayerName;

        AuthoritativePlayer.LocalPlayerStartEvent += player =>
        {
            gameObject.SetActive(false);
            LockCursor.Ins.enabled = true;
            LockCursor.Ins.Lock(true);
        };

        loginButton.onClick.AddListener(() =>
        {
            try
            {
                PlayerPrefs.SetString("AuthoritativePlayerName", PlayerName);
                var conn = NetworkManager.singleton.client.connection;
                ClientScene.Ready(conn);
                ClientScene.AddPlayer(conn, 0, new StringMessage(PlayerName));
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        });

        nameInput.onValueChanged.AddListener(value => { PlayerName = value; });

        LockCursor.Ins.Lock(false);
        LockCursor.Ins.enabled = false;
    }
}