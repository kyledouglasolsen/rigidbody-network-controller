﻿using UnityEngine;
using UnityEngine.UI;

public class UIDeath : MonoBehaviour
{
    [SerializeField] private Text respawnText;
    private Health health;
    private CanvasGroup group;
    private int finishRespawnTime;

    private void Awake()
    {
        group = GetComponent<CanvasGroup>();

        if (AuthoritativePlayer.Local != null)
        {
            OnStartLocalPlayer(AuthoritativePlayer.Local);
        }

        AuthoritativePlayer.LocalPlayerStartEvent += OnStartLocalPlayer;
        OnRespawnEvent();
    }

    private void OnDestroy()
    {
        AuthoritativePlayer.LocalPlayerStartEvent -= OnStartLocalPlayer;

        if (health != null)
        {
            health.OnDieEvent -= OnDieEvent;
            health.OnRespawnEvent -= OnRespawnEvent;
        }
    }

    private void Update()
    {
        var remainingSeconds = Mathf.Clamp((finishRespawnTime - NetworkTime.Current) / 1000f, 0f, health.RespawnSeconds);
        respawnText.text = $"Respawning in... {remainingSeconds:n0}";
    }

    private void OnStartLocalPlayer(AuthoritativePlayer player)
    {
        health = player.GetComponent<Health>();

        if (health != null)
        {
            health.OnDieEvent += OnDieEvent;
            health.OnRespawnEvent += OnRespawnEvent;
        }
    }

    private void OnRespawnEvent()
    {
        group.alpha = 0f;
        enabled = false;
    }

    private void OnDieEvent()
    {
        group.alpha = 1f;
        enabled = true;
        finishRespawnTime = NetworkTime.Current + (int)(health.RespawnSeconds * 1000);
    }
}