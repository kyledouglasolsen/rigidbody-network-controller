﻿using UnityEngine;
using UnityEngine.Networking;

public class Thrower : Item
{
    private const string Throw = "Throw", Idle = "Idle";
    private static int throwId = Animator.StringToHash(Throw), idleId = Animator.StringToHash(Idle);

    [SerializeField] private Throwable prefab;
    [SerializeField] private float velocity = 10f;
    [SerializeField] private float releaseSeconds = 0.5f;
    [SerializeField] private float cooldownSeconds = 1f;
    private Animator animator;
    private Renderer[] renderers;
    private int releaseMs, cooldownMs, nextThrowTime;

    private void Start()
    {
        animator = GetComponentInChildren<Animator>();
        renderers = animator.GetComponentsInChildren<Renderer>();

        releaseMs = (int)(releaseSeconds * 1000);
        cooldownMs = (int)(cooldownSeconds * 1000);
        nextThrowTime = NetworkTime.Current;
    }

    public override void UseDown(int useTime)
    {
        if (NetworkTime.Current < nextThrowTime)
        {
            return;
        }

        ThrowObject(useTime);
        nextThrowTime = NetworkTime.Current + cooldownMs + releaseMs;
    }

    public override void UseHeld(int useTime)
    {
        
    }

    public override void UseUp(int useTime)
    {
    }

    private async void ThrowObject(int useTime)
    {
        if (animator != null)
        {
            animator.Play(throwId);
        }

        var releaseTime = useTime + releaseMs;
        var releaseDelay = (releaseTime - NetworkTime.Current) / 1000f;

        if (releaseDelay > 0f)
        {
            await Timed.In(releaseDelay, null);
        }

        if (NetworkServer.active)
        {
            var instance = Instantiate(prefab, transform.position, Quaternion.identity);
            instance.Initialize(Owner, transform.forward * velocity);
            NetworkServer.Spawn(instance.gameObject);
        }

        SetRenderersEnabled(false);

        if (cooldownSeconds > 0f)
        {
            await Timed.In(cooldownSeconds, null);
        }

        SetRenderersEnabled(true);
    }

    private void SetRenderersEnabled(bool enable)
    {
        for (var i = 0; i < renderers.Length; ++i)
        {
            renderers[i].enabled = enable;
        }

        if (enable)
        {
            animator.Play(idleId);
        }
    }
}