﻿using UnityEngine;
using UnityEngine.Networking;

public class ItemUser : MonoBehaviour, INeedNetworkInput, ICareAboutDeath
{
    private NetworkIdentity identity;
    private Inventory inventory;
    private KeyInput previousKeys;

    public bool Enabled => enabled;
    public bool OnlyOwnerUpdate { get; } = false;

    public void OnInput(Vector2 mouseInput, KeyInput keyInput, float deltaTime)
    {
        var activeItem = inventory.GetActiveItem();

        if (activeItem == null)
        {
            return;
        }

        var useTime = identity.GetHistoryTime(NetworkWorldUpdater.InterpolateMilliseconds, NetworkWorldUpdater.FixedTimeStep);

        if (!previousKeys.HasFlag(KeyInput.UseItem) && keyInput.HasFlag(KeyInput.UseItem))
        {
            activeItem.UseDown(useTime);
        }

        if (keyInput.HasFlag(KeyInput.UseItem))
        {
            activeItem.UseHeld(useTime);
        }

        if (previousKeys.HasFlag(KeyInput.UseItem) && !keyInput.HasFlag(KeyInput.UseItem))
        {
            activeItem.UseUp(useTime);
        }

        previousKeys = keyInput;
    }

    private void Awake()
    {
        identity = GetComponent<NetworkIdentity>();
        inventory = GetComponent<Inventory>();
    }

    public void OnDie()
    {
        enabled = false;
    }

    public void OnRespawn()
    {
        enabled = true;
    }
}