﻿using UnityEngine;

public abstract class Item : MonoBehaviour
{
    public AuthoritativePlayer Owner { get; private set; }
    public Inventory Inventory { get; private set; }

    public abstract void UseDown(int useTime);
    public abstract void UseHeld(int useTime);
    public abstract void UseUp(int useTime);

    public void Initialize(AuthoritativePlayer owner, Inventory inventory)
    {
        Owner = owner;
        Inventory = inventory;
    }

    public void OnSetInactive()
    {
        gameObject.SetActive(false);
    }

    public void OnSetActive()
    {
        gameObject.SetActive(true);
    }
}