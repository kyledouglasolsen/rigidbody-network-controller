﻿using UnityEngine;

public class ItemChanger : MonoBehaviour, INeedNetworkInput
{
    private Inventory inventory;

    public bool Enabled => enabled;
    public bool OnlyOwnerUpdate { get; } = true;

    public void OnInput(Vector2 mouseInput, KeyInput keyInput, float deltaTime)
    {
        var newIndex = (int)inventory.CurrentActiveIndex;

        if (keyInput.HasFlag(KeyInput.NextItem))
        {
            newIndex += 1;
        }
        else if (keyInput.HasFlag(KeyInput.PreviousItem))
        {
            newIndex -= 1;
        }

        newIndex = (int)Mathf.Repeat(newIndex, inventory.Items.Length);

        if (newIndex != inventory.CurrentActiveIndex)
        {
            inventory.SetActiveIndex((uint)newIndex);
        }
    }

    private void Awake()
    {
        inventory = GetComponent<Inventory>();
    }
}