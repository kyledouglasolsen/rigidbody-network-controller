﻿//using System;
//using UnityEngine;
//
//public class Hook : Item
//{
//    [SerializeField] private Transform scaleTransform;
//    [SerializeField] private float travelSeconds = 1f;
//    [SerializeField] private float extendDistance = 10f;
//    private bool hooking;
//    private int startHookTime, finishHookTime, timeOffset;
//    private Vector3 previousHookPosition;
//    private Timed.EveryFrameStopper interpolateUpdate;
//    private Action onInterpolateUpdate;
//    private Action<float> onNetworkUpdate;
//
//    public override void UseDown(int useTime)
//    {
//        if (useTime < finishHookTime)
//        {
//            return;
//        }
//
//        hooking = true;
//        timeOffset = NetworkTime.Current - useTime;
//        startHookTime = useTime;
//        finishHookTime = useTime + (int)(travelSeconds * 1000f);
//        previousHookPosition = scaleTransform.position;
//        NetworkWorldUpdater.Subscribe(onNetworkUpdate);
//        interpolateUpdate = Timed.EveryFrame(onInterpolateUpdate);
//    }
//
//    public override void UseHeld(int useTime)
//    {
//    }
//
//    public override void UseUp(int useTime)
//    {
//    }
//
//    private void Awake()
//    {
//        onInterpolateUpdate = InterpolateUpdate;
//        onNetworkUpdate = NetworkUpdate;
//    }
//
//    private void NetworkUpdate(float deltaTime)
//    {
//        var lerp = CalculateLerp();
//        var newTargetPosition = GetTargetHookPosition(lerp);
//
//        if (lerp >= 1f)
//        {
//            hooking = false;
//            NetworkWorldUpdater.Unsubscribe(onNetworkUpdate);
//            interpolateUpdate.Cancel();
//            scaleTransform.localScale = new Vector3(1f, 1f, 0f);
//        }
//
//        previousHookPosition = newTargetPosition;
//    }
//
//    private void InterpolateUpdate()
//    {
//        var lerp = CalculateLerp();
//        var newScale = scaleTransform.localScale;
//        newScale.z = Mathf.Lerp(0f, extendDistance, lerp);
//        scaleTransform.localScale = newScale;
//    }
//
//    private float CalculateLerp()
//    {
//        if(!hooking)
//        {
//            return 0f;
//        }
//        
//        var current = NetworkTime.Current - timeOffset - startHookTime;
//        var target = finishHookTime - startHookTime;
//        return Mathf.Clamp01(current / (float)target);
//    }
//
//    private Vector3 GetTargetHookPosition(float lerp)
//    {
//        var newScale = Mathf.Lerp(0f, extendDistance, lerp);
//        return scaleTransform.position + scaleTransform.forward * newScale;
//    }
//}