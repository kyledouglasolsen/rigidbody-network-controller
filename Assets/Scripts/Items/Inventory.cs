﻿using UnityEngine;

public class Inventory : AuthoritativeWorldObject, IAmAuthoritative
{
    [SerializeField] private Item[] items;
    private AuthoritativePlayer player;
    private bool isIdle;

    public uint CurrentActiveIndex { get; private set; }
    public Item[] Items => items;
    public bool OnlyOwnerUpdate { get; } = true;

    public Item GetActiveItem()
    {
        return CurrentActiveIndex < items.Length ? items[CurrentActiveIndex] : null;
    }

    public void SetActiveIndex(uint activeIndex)
    {
        OnApplyState(new InventoryState(activeIndex));
    }

    public IAuthorityState GetState()
    {
        return new InventoryState(CurrentActiveIndex);
    }

    public void ApplyState(IAuthorityState state)
    {
        var inventoryState = (InventoryState)state;
        CurrentActiveIndex = inventoryState.ActiveIndex;
    }

    public void SerializeState(ByteWriter writer)
    {
        writer.WritePackedUInt32(CurrentActiveIndex);
    }

    public IAuthorityState DeserializeState(ByteReader reader)
    {
        return new InventoryState(reader.ReadPackedUInt32());
    }

    public override bool IsIdle()
    {
        return isIdle;
    }

    public override void ApplyEvent(ITimelineEvent timelineEvent)
    {
        OnApplyState((InventoryState)timelineEvent);
    }

    public override void AddEvent(int time, ITimelineEvent timelineEvent)
    {
        var previousEvent = (InventoryState)Timeline.LastEvent;
        var current = (InventoryState)timelineEvent;
        isIdle = previousEvent.ActiveIndex == current.ActiveIndex;

        Timeline.Trim(NetworkTime.Current - NetworkWorldUpdater.MaxHistoryMilliseconds);
        Timeline.Add(time, timelineEvent);
    }

    public override ITimelineEvent ToEvent()
    {
        return new InventoryState(CurrentActiveIndex);
    }

    public override void Write(ByteWriter writer)
    {
        writer.WritePackedUInt32(CurrentActiveIndex);
    }

    public override ITimelineEvent Read(ByteReader reader)
    {
        return new InventoryState(reader.ReadPackedUInt32());
    }

    protected override IHistoryEventTimeline CreateTimeline()
    {
        return new HistoryEventTimeline<InventoryState>();
    }

    protected override IHistoryEventInterpolate CreateInterpolate(IHistoryEventTimeline timeline)
    {
        return new HistoryEventInterpolate<InventoryState>((HistoryEventTimeline<InventoryState>)timeline, OnApplyState);
    }

    private void OnApplyState(InventoryState state)
    {
        if (CurrentActiveIndex != state.ActiveIndex)
        {
            var current = GetActiveItem();
            if (current != null)
            {
                current.OnSetInactive();
            }

            CurrentActiveIndex = state.ActiveIndex;

            current = GetActiveItem();
            if (current != null)
            {
                GetActiveItem().OnSetActive();
            }
        }
    }

    public override void OnDrawGizmosSelected()
    {
    }

    private void Awake()
    {
        player = GetComponent<AuthoritativePlayer>();

        for (var i = 0u; i < items.Length; ++i)
        {
            items[i].Initialize(player, this);

            if (i == CurrentActiveIndex)
            {
                items[i].OnSetActive();
            }
            else
            {
                items[i].OnSetInactive();
            }
        }
    }

    private struct InventoryState : IAuthorityState, ITimelineEvent<InventoryState>
    {
        public InventoryState(uint activeIndex)
        {
            ActiveIndex = activeIndex;
        }

        public readonly uint ActiveIndex;

        public int CalculateStateHash()
        {
            return ActiveIndex.GetHashCode();
        }

        public InventoryState InterpolateTo(InventoryState otherEvent, float lerp)
        {
            return new InventoryState(lerp >= 1f ? otherEvent.ActiveIndex : ActiveIndex);
        }

        public ITimelineEvent InterpolateTo(ITimelineEvent otherEvent, float lerp)
        {
            return InterpolateTo((InventoryState)otherEvent, lerp);
        }

        public ITimelineEvent ExtrapolateFrom(IHistoryEventTimeline timeline, float secondsSinceLastUpdate)
        {
            return new InventoryState(ActiveIndex);
        }
    }
}