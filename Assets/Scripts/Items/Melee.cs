﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Melee : Item
{
    [SerializeField] private LayerMask layerMask = Physics.DefaultRaycastLayers;
    [SerializeField] private Combo[] combos = new Combo[1];
    private readonly HashSet<Transform> hitTransforms = new HashSet<Transform>();
    private Animator animator;
    private ParticleImpacts particleImpacts;
    private Combo previousCombo;
    private Timed.EveryFrameStopper stopper;

    public override void UseDown(int useTime)
    {
        if (combos.Length == 0)
        {
            return;
        }

        AttackOnce(useTime);
    }

    public override void UseHeld(int useTime)
    {
    }

    public override void UseUp(int useTime)
    {
    }

    private void AttackOnce(int useTime)
    {
        if (!previousCombo.FinishedAttack(useTime))
        {
            return;
        }

        var comboIndex = Array.IndexOf(combos, previousCombo) + 1;

        if (comboIndex >= combos.Length)
        {
            comboIndex = 0;
        }

        var nextCombo = combos[comboIndex];

        if (!nextCombo.ValidCombo(useTime, previousCombo))
        {
            comboIndex = 0;
            nextCombo = combos[0];
        }

        nextCombo.Use(useTime, this);
        previousCombo = nextCombo;
    }

    private void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        particleImpacts = GetComponent<ParticleImpacts>();
        previousCombo = combos[0];
    }

    [Serializable]
    private class Combo
    {
        private static readonly List<IDamageable> Damageables = new List<IDamageable>();
        private static readonly RaycastHit[] Hits = new RaycastHit[25];

        [SerializeField] private float secondsToCombo = 0.5f;
        [SerializeField] private float secondsBeforeMeleeDamage = 0.25f;
        [SerializeField] private float secondsAfterMeleeDamage = 0.25f;
        [SerializeField] private int collisionActiveFrameCount = 10;
        [SerializeField] private float radius = 0.25f;
        [SerializeField] private float distance = 2f;
        [SerializeField] private string stateName = "Melee";
        private int lastAttackTime, finishAttackTime, stateId = -1;

        public bool FinishedAttack(int useTime)
        {
            return useTime >= finishAttackTime;
        }

        public bool ValidCombo(int useTime, Combo previousCombo)
        {
            var minMsToActivate = (int)(previousCombo.secondsBeforeMeleeDamage * 1000f);
            var maxMsToActivate = (int)((secondsBeforeMeleeDamage + secondsToCombo) * 1000f);
            var minTargetTime = previousCombo.lastAttackTime + minMsToActivate;
            var maxTargetTime = previousCombo.lastAttackTime + maxMsToActivate;
            return useTime >= minTargetTime && useTime <= maxTargetTime;
        }

        public async void Use(int useTime, Melee melee)
        {
            if (stateId == -1)
            {
                stateId = Animator.StringToHash(stateName);
            }

            melee.stopper.Cancel();
            melee.animator.CrossFade(stateId, 0.1f);
            finishAttackTime = useTime + (int)((secondsBeforeMeleeDamage + secondsAfterMeleeDamage) * 1000f);
            lastAttackTime = useTime;
            var timeOffset = NetworkTime.Current - useTime;

            if (secondsBeforeMeleeDamage > 0f)
            {
                await Timed.In(secondsBeforeMeleeDamage, null);
            }

            melee.hitTransforms.Clear();

            melee.stopper = Timed.EveryFixedFrame(() =>
            {
                var currentTime = NetworkTime.Current - timeOffset;
                var meleeTransform = melee.transform;
                var ray = new Ray(meleeTransform.position, meleeTransform.forward);

                AuthoritativePhysics.RollBack(currentTime);

                var count = AuthoritativePhysics.SphereCastAll(currentTime, ray, radius, Hits, distance, melee.layerMask, QueryTriggerInteraction.Ignore);

                for (var i = 0; i < count; ++i)
                {
                    var hit = Hits[i];

                    if (melee.hitTransforms.Contains(hit.transform) || hit.transform == meleeTransform || hit.transform.IsChildOf(meleeTransform) || meleeTransform.IsChildOf(hit.transform))
                    {
                        continue;
                    }

                    melee.hitTransforms.Add(hit.transform);

                    Damageables.Clear();
                    hit.collider.GetComponents(Damageables);

                    for (var j = 0; j < Damageables.Count; ++j)
                    {
                        Damageables[j].DoDamage(new DamageInfo(melee.Owner, 1, hit.collider.transform.InverseTransformPoint(hit.point)));
                    }

                    melee.particleImpacts.TryCreateImpactParticle(hit);
                }

                AuthoritativePhysics.Resume();
            }, collisionActiveFrameCount);
        }
    }
}