﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Gun : Item
{
    private static readonly List<IDamageable> Damageables = new List<IDamageable>();
    private static readonly RaycastHit[] Hits = new RaycastHit[100];

    [SerializeField] private LayerMask layerMask = Physics.DefaultRaycastLayers;
    [SerializeField] private float delayBetweenShots = 0.1f;
    [SerializeField] private int bulletsPerShot = 1;
    [SerializeField] [Range(0f, 1f)] private float accuracy = 1f;
    [SerializeField] private float maxSpreadAngle = 10f;
    [SerializeField] private float forceToApply = 1f;
    private int nextFireTime;
    private float lineRendererWidth;
    private ParticleImpacts particleImpacts;
    private LineRenderer lineRenderer;

    public override void UseDown(int useTime)
    {
    }

    public override void UseHeld(int useTime)
    {
        if (NetworkTime.Current < nextFireTime)
        {
            return;
        }

        var baseRay = new Ray(transform.position, transform.forward);
        AuthoritativePhysics.RollBack(useTime);
        
        for (var i = 0; i < bulletsPerShot; ++i)
        {
            FireOnce(baseRay, useTime);
        }
        
        AuthoritativePhysics.Resume();
    }

    public override void UseUp(int useTime)
    {
    }

    private void FireOnce(Ray baseRay, int useTime)
    {
        var inverseAccuracy = 1f - accuracy;
        var randomRotation = Quaternion.Euler(Random.Range(-maxSpreadAngle, maxSpreadAngle) * inverseAccuracy, Random.Range(-maxSpreadAngle, maxSpreadAngle) * inverseAccuracy, 0f);
        var ray = new Ray(baseRay.origin, randomRotation * baseRay.direction);

        var count = AuthoritativePhysics.RaycastAll(useTime, ray, Hits, 1000f, layerMask);
        var endPosition = ray.origin + ray.direction * 1000f;

        Array.Sort(Hits, 0, count, RaycastDistanceComparer.Instance);

        for (var i = 0; i < count; ++i)
        {
            var hit = Hits[i];

            if (hit.transform == transform || hit.transform.IsChildOf(transform) || transform.IsChildOf(hit.transform))
            {
                continue;
            }

            Damageables.Clear();
            hit.collider.GetComponents(Damageables);

            for (var j = 0; j < Damageables.Count; ++j)
            {
                Damageables[j].DoDamage(new DamageInfo(Owner, 1, hit.collider.transform.InverseTransformPoint(hit.point)));
            }

            var rBody = hit.rigidbody;
            if (rBody != null)
            {
                rBody.AddForce(ray.direction * forceToApply, ForceMode.Impulse);
            }

            particleImpacts.TryCreateImpactParticle(hit);

            endPosition = hit.point;
            break;
        }

        nextFireTime = (int)(NetworkTime.Current + delayBetweenShots * 1000f);

        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1, endPosition);
        lineRenderer.endWidth = lineRenderer.startWidth = lineRendererWidth;
    }

    private void Awake()
    {
        particleImpacts = GetComponent<ParticleImpacts>();
        lineRenderer = GetComponentInChildren<LineRenderer>();
        lineRendererWidth = lineRenderer.startWidth;
    }

    private void Update()
    {
        var newWidth = Mathf.Clamp(lineRenderer.startWidth - Time.smoothDeltaTime, 0f, lineRendererWidth);
        lineRenderer.startWidth = lineRenderer.endWidth = newWidth;
    }
}