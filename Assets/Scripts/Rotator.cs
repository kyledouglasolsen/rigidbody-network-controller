﻿using UnityEngine;

public class Rotator : MonoBehaviour, INeedNetworkInput, IAmAuthoritative, ICareAboutDeath
{
    [SerializeField] private float rotateSpeed = 5f;
    [SerializeField] private Transform xTransform = null, yTransform = null;

    public float X { get; private set; }
    public float Y { get; private set; }

    public bool Enabled => enabled;
    public bool OnlyOwnerUpdate { get; } = true;

    public void OnInput(Vector2 mouseInput, KeyInput keyInput, float deltaTime)
    {
        var rotate = mouseInput * rotateSpeed * deltaTime;

        if (xTransform != null && !Mathf.Approximately(mouseInput.y, 0f))
        {
            X = WrapAngle(X - rotate.y);
            xTransform.localRotation = Quaternion.Euler(X, 0f, 0f);
        }

        if (yTransform != null && !Mathf.Approximately(mouseInput.x, 0f))
        {
            Y = WrapAngle(Y + rotate.x);
            yTransform.localRotation = Quaternion.Euler(0f, Y, 0f);
        }
    }

    public IAuthorityState GetState()
    {
        return new RotatorState(X, Y);
    }

    public void ApplyState(IAuthorityState state)
    {
        var rotatorState = (RotatorState)state;

        if (xTransform != null)
        {
            X = rotatorState.X;
            xTransform.localRotation = Quaternion.Euler(X, 0f, 0f);
        }

        if (yTransform != null)
        {
            Y = rotatorState.Y;
            yTransform.localRotation = Quaternion.Euler(0f, Y, 0f);
        }
    }

    public void SerializeState(ByteWriter writer)
    {
        if (xTransform != null)
        {
            writer.Write(X);
        }

        if (yTransform != null)
        {
            writer.Write(Y);
        }
    }

    public IAuthorityState DeserializeState(ByteReader reader)
    {
        return new RotatorState(xTransform != null ? reader.ReadFloat() : 0f, yTransform != null ? reader.ReadFloat() : 0f);
    }

    public void OnDie()
    {
        enabled = false;
    }

    public void OnRespawn()
    {
        enabled = true;
    }

    private static float WrapAngle(float angle)
    {
        while (angle > 360f)
        {
            angle -= 360f;
        }

        while (angle < 0f)
        {
            angle += 360f;
        }

        return angle;
    }

    public struct RotatorState : IAuthorityState
    {
        public RotatorState(float x, float y)
        {
            X = x;
            Y = y;
        }

        public readonly float X, Y;

        public int CalculateStateHash()
        {
            return X.GetHashCode() ^ Y.GetHashCode();
        }
    }
}