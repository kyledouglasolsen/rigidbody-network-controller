﻿using UnityEngine;
using UnityEngine.Networking;

public class StunGrenade : Grenade
{
    [SerializeField] private float slowPercent = 0.5f;
    [SerializeField] private float slowSeconds = 5f;

    protected override void OnHitCollider(AuthoritativePlayer owner, Collider hitCollider, Vector3 worldPoint, Vector3 directionTo, float distancePercent)
    {
        var buffManager = hitCollider.GetComponentInParent<BuffManager>();

        if (buffManager == null)
        {
            return;
        } 

        var networkSlowEndTime = NetworkTime.Current + (int)(slowSeconds * 1000f);
        
        if (!NetworkClient.active)
        {
            buffManager.Add(new SlowDebuff(slowPercent, networkSlowEndTime));
        } 

        RpcApplyBuffToGameObject(buffManager.gameObject, networkSlowEndTime);
    }

    [ClientRpc]
    private void RpcApplyBuffToGameObject(GameObject target, int networkSlowEndTime)
    {
        if (target == null)
        {
            return;
        }

        var buffManager = target.GetComponent<BuffManager>();

        if (buffManager == null)
        {
            return;
        }

        if (networkSlowEndTime > NetworkTime.Current)
        {
            buffManager.Add(new SlowDebuff(slowPercent, networkSlowEndTime));
        }
    }
}